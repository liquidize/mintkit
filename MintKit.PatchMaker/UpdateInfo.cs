﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintKit.Client.Core
{
    public class UpdateInfoChange
    {
        public string Description { get; set; }
        public string Type { get; set; }
        public string Component { get; set; }
    }

    public class UpdateInfo
    {
        public string LauncherVersion { get; set; }
        public string VersionName { get; set; }
        public string Version { get; set; }
        public string Date { get; set; }
        public List<UpdateInfoChange> ChangeSet { get; set; }
    }
}
