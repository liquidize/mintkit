﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MintKit.Client.Core;
using MintKit.Loader;
using Newtonsoft.Json;

namespace MintKit.PatchMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<FileInfo> Files { get; set; }
        private Dictionary<string, string> HashDictionary { get; set; }
        private UpdateInfo info = new UpdateInfo();

        public MainWindow()
        {
            InitializeComponent();
            info.ChangeSet = new List<UpdateInfoChange>();
            updateTypeBox.Items.Add("Fix");
            updateTypeBox.Items.Add("New");
            updateTypeBox.Items.Add("Change");
            updateTypeBox.Items.Add("Removed");

            updateCompBox.Items.Add("Client");
            updateCompBox.Items.Add("Core Plugins");
            updateCompBox.Items.Add("Api");
            updateCompBox.Items.Add("Loader");
            

        }

        private void browseBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Ookii.Dialogs.Wpf.VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            {
                Ookii.Dialogs.Wpf.VistaFolderBrowserDialog dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                dialog.ShowDialog();
                if (dialog.SelectedPath != String.Empty)
                {
                    folderText.Text = dialog.SelectedPath;
                }
            }
            else
            {

            }
        }

        private void getDataBtn_Click(object sender, RoutedEventArgs e)
        {
            HashDictionary = new Dictionary<string, string>();
            if (Directory.Exists(folderText.Text))
            {
                Files = new DirectoryInfo(folderText.Text).GetFiles("*.*", SearchOption.AllDirectories).ToList();
                for (int i = 0; i < Files.Count; i++)
                {
                    fileList.Items.Add(Files[i].FullName.Replace(folderText.Text, "").Remove(0, 1));
                   HashDictionary.Add(Files[i].FullName.Replace(folderText.Text, "").Remove(0, 1).Replace(@"\\",@"\"),GetMD5HashFromFile(Files[i].FullName));
                }
            }
            else
            {
                MessageBox.Show("Invalid Directory!");
            }
        }

        protected string GetMD5HashFromFile(string fileName)
        {
            FileStream file = new FileStream(fileName, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private void buildBtn_Click(object sender, RoutedEventArgs e)
        {
           File.WriteAllText("releasehashes.json",JsonConvert.SerializeObject(HashDictionary,Formatting.Indented));
        }

        private void addVersionInfo_Click(object sender, RoutedEventArgs e)
        {
            info.ChangeSet.Add(new UpdateInfoChange() {Component = updateCompBox.SelectedItem.ToString(), Description = updateInfoDescBox.Text, Type = updateTypeBox.SelectedItem.ToString()});
        }

        private void buildVersion_Click(object sender, RoutedEventArgs e)
        {
            info.Version = versionBox.Text;
            info.VersionName = versionNameBox.Text;
            info.LauncherVersion = GetMD5HashFromFile("MintKit.Loader.exe");
            info.Date = DateTime.Today.ToShortDateString();
            File.WriteAllText("version.json",JsonConvert.SerializeObject(info,Formatting.Indented));
        }
    }
}
