﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.CompilerServices;

using Newtonsoft.Json;

namespace MintKit.Loader
{
   public class MintKitReleaseHashes
    {
        public Dictionary<string,string> FileHashes { get; set; }

        public static MintKitReleaseHashes Instance { get; set; }

       public MintKitReleaseHashes()
       {
           
       }

       public void Serialize(string tofile)
       {
           File.WriteAllText(tofile,JsonConvert.SerializeObject(this,Formatting.Indented));
       }

       public static void Deserialize(string json)
       {
           try
           {
               Instance = JsonConvert.DeserializeObject<MintKitReleaseHashes>(json);
           }
           catch (Exception e)
           {

           }
       }

    }
}
