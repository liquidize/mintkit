﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MintKit.Client;
using MintKit.Common.Logging;
using Newtonsoft.Json;

namespace MintKit.Loader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        private Queue<string> DownloadUrls = new Queue<string>(); 
        private Dictionary<string,string> _currenthashes = new Dictionary<string, string>();
        private string _baseDirectory = Directory.GetCurrentDirectory() + @"\";
        private int currentTotalProgress = 0;
        private int totalProgressCount = 0;
        private string currentDownload = "";
        public bool autoUpdate = false;

        public MainWindow()
        {
            InitializeComponent();

            optionsGroup.Margin = new Thickness(403,53,-181,0);
            debugBox.IsChecked = MintKitLoaderOptions.Instance.DebugMode;

            this.Loaded += OnLoaded;

        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (autoUpdate)
            {
                AutoUpdate();
            }
        }

        protected string GetMd5HashFromFile(string fileName)
        {
            FileStream file = new FileStream(fileName, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private async Task<string> GetHashes()
        {
            using (WebClient client = new WebClient())
            {
                return await client.DownloadStringTaskAsync(MintKitLoaderOptions.Instance.UpdateHost + "releasehashes.json");
            }

        }


        private void DownloadFile()
        {
            if (DownloadUrls.Any())
            {
                using (WebClient client = new WebClient())
                {

                    var file = DownloadUrls.Dequeue();
                    currentDownload = file;
                    client.DownloadDataCompleted += (sender, args) =>
                    {
                        if (args.Error != null)
                        {
                            // handle error scenario
                            throw args.Error;
                        }
                        if (args.Cancelled)
                        {
                            // handle cancelled scenario
                        }
                        if (Directory.Exists(_baseDirectory + currentDownload))
                        {
                            File.WriteAllBytes(currentDownload, args.Result);
                        }
                        else
                        {
                            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(_baseDirectory + currentDownload));
                            File.WriteAllBytes(currentDownload, args.Result);
                        }
                        DownloadFile();
                    };

                    client.DownloadProgressChanged += (sender, args) =>
                    {
                        currentProgressBar.Value = (args.TotalBytesToReceive/args.BytesReceived)*100;
                    };
                    

                    client.DownloadDataAsync(new Uri(MintKitLoaderOptions.Instance.UpdateHost + file), file);
                    if (MintKitLoaderOptions.Instance.DebugMode)
                    {
                        this.Log().Debug("Downloading: {0}",currentDownload);
                    }
                    currentTotalProgress += 1;
                    currentProgressLbl.Content = "Downloading... " + file;
                    totalProgressLbl.Content = string.Format("Total Progress: {0}/{1}", currentTotalProgress,
                        totalProgressCount);
                    totalProgressBar.Value += 1;
                }

            }
            else
            {
                currentProgressLbl.Content = "Completed.";
                totalProgressLbl.Content = "Completed.";

                if (File.Exists("MintKit.Client.exe"))
                {
                    Process.Start("MintKit.Client.exe");
                   Application.Current.Shutdown();
                }
                else
                {
                    launchBtn.IsEnabled = true;
                    currentProgressLbl.Content = "MintKit Client Executable does not exists!";
                    this.Log().Error("MintKit Client Executable Not Found");
                }
            }
        }


        private async void launchBtn_Click(object sender, RoutedEventArgs e)
        {
            launchBtn.IsEnabled = false;
            if (MintKitLoaderOptions.Instance.DebugMode)
            {
                this.Log().Debug("Starting update process.");
            }

            currentProgressLbl.Content = "Downloading Current Hashes...";
            Task<string> hashTask = GetHashes();
            string data = await hashTask;

            if (hashTask.IsCanceled || hashTask.IsFaulted)
            {
                currentProgressLbl.Content = "An error occurred. Failed to download latest hash set.";

                if (MintKitLoaderOptions.Instance.DebugMode)
                {
                    if (hashTask.Exception != null)
                    {
                        this.Log().Fatal(hashTask.Exception.ToString);
                    }
                }

                launchBtn.IsEnabled = true;

            }

            if (string.IsNullOrEmpty(data) != true)
            {
                try
                {
                    _currenthashes = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                }
                catch (Exception jsonEx)
                {
                    currentProgressLbl.Content = "Failed to deserialize latest hash set!";

                    if (MintKitLoaderOptions.Instance.DebugMode)
                    {
                        this.Log().Fatal(jsonEx.ToString);
                    }
                    launchBtn.IsEnabled = true;
                }
            }

            if (_currenthashes.Count > 0)
            {
                string[] files = Directory.GetFiles(_baseDirectory, "*.*", SearchOption.AllDirectories);

                foreach (KeyValuePair<string, string> kvp in _currenthashes)
                {
                    if (string.IsNullOrEmpty(files.FirstOrDefault(n => n.Replace(_baseDirectory, "") == kvp.Key)) != true)
                    {
                        string file = files.First(n => n.Replace(_baseDirectory, "") == kvp.Key);
                            
                        string ourHash = GetMd5HashFromFile(file);

                        if (string.IsNullOrEmpty(ourHash) != true)
                        {
                            if (ourHash != kvp.Value)
                            {
                                DownloadUrls.Enqueue(kvp.Key);
                            }
                        }
                        else
                        {
                            if (MintKitLoaderOptions.Instance.DebugMode)
                            {
                                this.Log().Warn("Failed to hash: {0}", file);
                            }
                            DownloadUrls.Enqueue(kvp.Key);
                        }

                    }
                    else
                    {
                      //  MessageBox.Show("File does not exists: " + kvp.Key);
                        if (MintKitLoaderOptions.Instance.DebugMode)
                        {
                            this.Log().Info("File {0} does not exists. Adding to download queue.", kvp.Key);
                        }
                        DownloadUrls.Enqueue(kvp.Key);
                    }
                }

                // Begin downloading the files.

                if (DownloadUrls.Count > 0)
                {
                    totalProgressBar.Maximum = DownloadUrls.Count;
                    totalProgressCount = DownloadUrls.Count;
                    DownloadFile();
                }
                else
                {
                    currentProgressLbl.Content = "Completed.";
                    totalProgressLbl.Content = "Completed.";

                    if (File.Exists("MintKit.Client.exe"))
                    {
                        Process.Start("MintKit.Client.exe");
                      Application.Current.Shutdown();
                    }
                    else
                    {
                        launchBtn.IsEnabled = true;
                        currentProgressLbl.Content = "MintKit Client Executable does not exists!";
                        this.Log().Error("MintKit Client Executable Not Found");
                    }
                }



            }
            else
            {
                currentProgressLbl.Content = "Current hash set count is < 1. Something went wrong!";

                if (MintKitLoaderOptions.Instance.DebugMode)
                {
                    this.Log().Error(_currenthashes.ToString);
                }
                launchBtn.IsEnabled = true;
            }




        }

        private void Options_MouseDown(object sender, MouseButtonEventArgs e)
        {
            OptionsPath.Fill = (SolidColorBrush) FindResource("GlyphPressedBackgroundBrush");

        }

        private void Options_MouseUp(object sender, MouseButtonEventArgs e)
        {
          
            OptionsPath.Fill = (SolidColorBrush) FindResource("GlyphBackgroundBrush");

        }

        private void debugBox_Checked(object sender, RoutedEventArgs e)
        {
            MintKitLoaderOptions.Instance.DebugMode = true;
            MintKitLoaderOptions.Instance.Serialize();

        }

        private void debugBox_UnChecked(object sender, RoutedEventArgs e)
        {
            MintKitLoaderOptions.Instance.DebugMode = false;
            MintKitLoaderOptions.Instance.Serialize();
        }

        private async void AutoUpdate()
        {

            launchBtn.IsEnabled = false;
            if (MintKitLoaderOptions.Instance.DebugMode)
            {
                this.Log().Debug("Starting update process.");
            }

            currentProgressLbl.Content = "Downloading Current Hashes...";
            Task<string> hashTask = GetHashes();
            string data = await hashTask;

            if (hashTask.IsCanceled || hashTask.IsFaulted)
            {
                currentProgressLbl.Content = "An error occurred. Failed to download latest hash set.";

                if (MintKitLoaderOptions.Instance.DebugMode)
                {
                    if (hashTask.Exception != null)
                    {
                        this.Log().Fatal(hashTask.Exception.ToString);
                    }
                }

                launchBtn.IsEnabled = true;

            }

            if (string.IsNullOrEmpty(data) != true)
            {
                try
                {
                    _currenthashes = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                }
                catch (Exception jsonEx)
                {
                    currentProgressLbl.Content = "Failed to deserialize latest hash set!";

                    if (MintKitLoaderOptions.Instance.DebugMode)
                    {
                        this.Log().Fatal(jsonEx.ToString);
                    }
                    launchBtn.IsEnabled = true;
                }
            }

            if (_currenthashes.Count > 0)
            {
                string[] files = Directory.GetFiles(_baseDirectory, "*.*", SearchOption.AllDirectories);

                foreach (KeyValuePair<string, string> kvp in _currenthashes)
                {
                    if (string.IsNullOrEmpty(files.FirstOrDefault(n => n.Replace(_baseDirectory, "") == kvp.Key)) != true)
                    {
                        string file = files.First(n => n.Replace(_baseDirectory, "") == kvp.Key);

                        string ourHash = GetMd5HashFromFile(file);

                        if (string.IsNullOrEmpty(ourHash) != true)
                        {
                            if (ourHash != kvp.Value)
                            {
                                DownloadUrls.Enqueue(kvp.Key);
                            }
                        }
                        else
                        {
                            if (MintKitLoaderOptions.Instance.DebugMode)
                            {
                                this.Log().Warn("Failed to hash: {0}", file);
                            }
                            DownloadUrls.Enqueue(kvp.Key);
                        }

                    }
                    else
                    {
                        //  MessageBox.Show("File does not exists: " + kvp.Key);
                        if (MintKitLoaderOptions.Instance.DebugMode)
                        {
                            this.Log().Info("File {0} does not exists. Adding to download queue.", kvp.Key);
                        }
                        DownloadUrls.Enqueue(kvp.Key);
                    }
                }

                // Begin downloading the files.

                if (DownloadUrls.Count > 0)
                {
                    totalProgressBar.Maximum = DownloadUrls.Count;
                    totalProgressCount = DownloadUrls.Count;
                    DownloadFile();
                }
                else
                {
                    currentProgressLbl.Content = "Completed.";
                    totalProgressLbl.Content = "Completed.";

                    if (File.Exists("MintKit.Client.exe"))
                    {
                        Process.Start("MintKit.Client.exe");
                        Application.Current.Shutdown();
                    }
                    else
                    {
                        launchBtn.IsEnabled = true;
                        currentProgressLbl.Content = "MintKit Client Executable does not exists!";
                        this.Log().Error("MintKit Client Executable Not Found");
                    }
                }



            }
            else
            {
                currentProgressLbl.Content = "Current hash set count is < 1. Something went wrong!";

                if (MintKitLoaderOptions.Instance.DebugMode)
                {
                    this.Log().Error(_currenthashes.ToString);
                }
                launchBtn.IsEnabled = true;
            }

        }
    }
}
