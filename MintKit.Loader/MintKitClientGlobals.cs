﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MintKit.Common.Logging;
using Newtonsoft.Json;

namespace MintKit.Client
{
    /// <summary>
    /// Global options, used in every profile.
    /// </summary>
    public class MintKitOptions
    {
        public bool DebugMode { get; set; }

        public string AccentColor { get; set; }

        public string Theme { get; set; }

        public string Version { get; set; }

        public static MintKitOptions Instance { get; set; }

        public void Serialize()
        {
            try
            {
                File.WriteAllText("mintkitclientglobals.json", JsonConvert.SerializeObject(this, Formatting.Indented));
            }
            catch (Exception e)
            {
                if (this.DebugMode)
                {
                    this.Log().Error(e.ToString);
                }
            }
        }

        public static void Deserialize()
        {
            try
            {
                Instance = JsonConvert.DeserializeObject<MintKitOptions>(File.ReadAllText("mintkitclientglobals.json"));
            }
            catch (Exception)
            {

                Instance = new MintKitOptions() { DebugMode = true, Version = " 1.0.0" };
            }
        }
    }
}
