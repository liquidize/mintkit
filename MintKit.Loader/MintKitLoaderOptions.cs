﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using MintKit.Common.Logging;

namespace MintKit.Loader
{
    public class MintKitLoaderOptions
    {
        public bool DebugMode { get; set; }
        public string UpdateHost { get; set; }

        public static MintKitLoaderOptions Instance { get;  set; }

        public MintKitLoaderOptions()
        {
            
        }

        public void Serialize()
        {
            File.WriteAllText("mintkitloader.json",JsonConvert.SerializeObject(this,Formatting.Indented));
        }

        public static void Deserialize()
        {
            try
            {
                Instance = JsonConvert.DeserializeObject<MintKitLoaderOptions>(File.ReadAllText("mintkitloader.json"));
            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to deserialize MintKit Loader Options. Creating a new one with default settings.");

                Instance = new MintKitLoaderOptions() { DebugMode = true, UpdateHost = "http://ffxivgoblin.com/mintkit/updates/release/" };
                Instance.Serialize();
                // Log this shit.
                Log.GetLoggerFor(typeof(MintKitLoaderOptions).Name).Error(e.ToString);

            }
        }

    }
}
