﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MintKit.Common.Logging;

namespace MintKit.Loader
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MainWindow Window;

        protected override void OnStartup(StartupEventArgs e)
        {
            // Iinitalize Logging System
            Log.InitializeWith<Log4NetLog>();
            // Load Settings, if they exist
            MintKitLoaderOptions.Deserialize();
            Window = new MainWindow();
            if (e.Args.Length > 0)
            {
                if (e.Args[0] == "update")
                {
                    
                    Window.autoUpdate = true;
                }
            }
            Window.Show();
           base.OnStartup(e);
        }
    }
}
