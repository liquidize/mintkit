﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MintKit.Client.SharpBucket.V1;
using MintKit.Client.SharpBucket.V1.EndPoints;
using MintKit.Client.SharpBucket.V1.Pocos;
using MahApps.Metro;

namespace MintKit.Client.UserControls
{
    /// <summary>
    /// </summary>
    /// </summary>
    public partial class NewIssueWindow
    {

        private SharpBucketV1 bucket = new SharpBucketV1();
        public NewIssueWindow()
        {
            InitializeComponent();

            bucket.OAuth2LeggedAuthentication("t9mm9F3tUkTmWQZfWH", "V8RTPZjjkpSGA96AfxdkwFaWDgym5ML5");

            var repos = bucket.RepositoriesEndPoint("liquidize", "mintkit");
            var components = repos.ListComponents();

            for (int i = 0; i < components.Count; i++)
            {
                this.componentBox.Items.Add(components[i].name);
            }

            kindBox.Items.Add("bug");
            kindBox.Items.Add("enhancement");
            kindBox.Items.Add("proposal");
            kindBox.Items.Add("task");

            priorityBox.Items.Add("trivial");
            priorityBox.Items.Add("minor");
            priorityBox.Items.Add("major");
            priorityBox.Items.Add("critical");
            priorityBox.Items.Add("blocker");
        }

        private void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(titleTextbox.Text))
            {
                MessageBox.Show("Please input a title!");
                return;
            }

            if (string.IsNullOrEmpty(descriptiontextBox.Text))
            {
                MessageBox.Show("Please input a description of the issue!");
                return;
            }

            if (priorityBox.SelectedItem == null)
            {
                MessageBox.Show("Please select a priority you beleive reasonable.");
                return;
            }

            if (kindBox.SelectedItem == null)
            {
                MessageBox.Show("Please select a kind of issue.");
                return;
            }

            if (componentBox.SelectedItem == null)
            {
                MessageBox.Show("Please select a component for the issue.");
                return;
            }
            var bucket = new SharpBucketV1();
            bucket.OAuth2LeggedAuthentication("t9mm9F3tUkTmWQZfWH", "V8RTPZjjkpSGA96AfxdkwFaWDgym5ML5");

            var repos = bucket.RepositoriesEndPoint("liquidize", "mintkit");
            var issueresource = repos.IssuesResource();
            issueresource.PostIssue(new SharpBucket.V1.Pocos.Issue
            {
                title = titleTextbox.Text,
                content = descriptiontextBox.Text,
                priority = priorityBox.SelectedItem.ToString(),
                metadata =
                new Metadata
                {

                    component = componentBox.SelectedItem.ToString(),
                    kind = kindBox.SelectedItem.ToString()
                }
            });

            MessageBox.Show("Your issue has been created. Thank you!");
            this.Close();
        }
    }
}
