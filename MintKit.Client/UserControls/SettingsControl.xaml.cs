﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MahApps.Metro;
using MintKit.Api;
using MintKit.Api.Plugin;
using MintKit.Api.Ui;
using MintKit.Api.Ui.Controls;
using MintKit.Client.Core;
using MintKit.Client.SharpBucket;
using MintKit.Client.SharpBucket.V1;
using MintKit.Common.Logging;
using Newtonsoft.Json;

namespace MintKit.Client.UserControls
{
    /// <summary>
    /// Interaction logic for SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        private List<SettingsPluginInfo> _pluginInfos = new List<SettingsPluginInfo>();
        private List<SettingsIssueInfo> _issueInfos = new List<SettingsIssueInfo>();
        private DispatcherTimer updateTimer;
        private UpdateInfo _updateInfo;

        public SettingsControl()
        {
            InitializeComponent();
            updateTimer = new DispatcherTimer(TimeSpan.FromMinutes(15), DispatcherPriority.Background, UpdateCallback, Dispatcher.CurrentDispatcher);

            if (MintKitOptions.Instance.AutoUpdateCheck)
                updateTimer.Start();

            _updateInfo = new UpdateInfo();

            accentComboBox.Items.Add("Emerald");
            accentComboBox.Items.Add("Blue");
            accentComboBox.Items.Add("Cobalt");
            accentComboBox.Items.Add("Sienna");
            accentComboBox.Items.Add("Yellow");
            accentComboBox.Items.Add("Lime");
            accentComboBox.Items.Add("Green");
            accentComboBox.Items.Add("Orange");
            accentComboBox.Items.Add("Red");
            accentComboBox.Items.Add("Crimson");
            accentComboBox.Items.Add("Amber");
            accentComboBox.Items.Add("Violet");
            accentComboBox.Items.Add("Magenta");
            accentComboBox.Items.Add("Steel");



            themeComboBox.Items.Add("BaseDark");
            themeComboBox.Items.Add("BaseLight");

            debugModeToggle.IsChecked = MintKitOptions.Instance.DebugMode;
            automaticUpdatesToggle.IsChecked = MintKitOptions.Instance.AutoUpdateCheck;
            cacheImagesToggle.IsChecked = MintKitOptions.Instance.CacheImages;
            accentComboBox.SelectedItem = MintKitOptions.Instance.AccentColor != "" ? MintKitOptions.Instance.AccentColor : "Emerald";
            themeComboBox.SelectedItem = MintKitOptions.Instance.Theme != "" ? MintKitOptions.Instance.Theme : "BaseDark";
            foreach (IMintKitPluginBase pluginInfo in PluginManager.Plugins)
            {
                _pluginInfos.Add(new SettingsPluginInfo() { Name = pluginInfo.PluginName, Author = pluginInfo.PluginAuthor, Version = pluginInfo.Version, Description = pluginInfo.Description });
            }
            pluginlistView.ItemsSource = _pluginInfos;

        }

        private void accentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // get the theme from the current application
            var theme = ThemeManager.DetectAppStyle(Application.Current);

            // now set the Green accent and dark theme
            ThemeManager.ChangeAppStyle(Application.Current,
                                        ThemeManager.GetAccent(accentComboBox.SelectedItem.ToString()),
                                        ThemeManager.GetAppTheme(theme.Item1.Name));

            MintKitOptions.Instance.AccentColor = accentComboBox.SelectedItem.ToString();
            //MintKitOptions.Instance.Theme = themeComboBox.SelectedItem.ToString();
            MintKitOptions.Instance.Serialize();
        }


        private void themeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // get the theme from the current application
            var theme = ThemeManager.DetectAppStyle(Application.Current);

            // now set the Green accent and dark theme
            ThemeManager.ChangeAppStyle(Application.Current,
                                        ThemeManager.GetAccent(theme.Item2.Name),
                                        ThemeManager.GetAppTheme(themeComboBox.SelectedItem.ToString()));

            MintKitOptions.Instance.Theme = themeComboBox.SelectedItem.ToString();
            MintKitOptions.Instance.Serialize();
        }


        private void debugModeToggle_Checked(object sender, RoutedEventArgs e)
        {
            MintKitOptions.Instance.DebugMode = true;
            MintKitOptions.Instance.Serialize();
        }

        private void debugModeToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            MintKitOptions.Instance.DebugMode = false;
            MintKitOptions.Instance.Serialize();
        }


        private void cacheToggle_Checked(object sender, RoutedEventArgs e)
        {
            MintKitOptions.Instance.CacheImages = true;
            MintKitOptions.Instance.Serialize();
        }

        private void cacheToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            MintKitOptions.Instance.CacheImages = false;
            MintKitOptions.Instance.Serialize();
        }



        private void updateToggle_Checked(object sender, RoutedEventArgs e)
        {
            MintKitOptions.Instance.AutoUpdateCheck = true;
            MintKitOptions.Instance.Serialize();
        }

        private void updateToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            MintKitOptions.Instance.AutoUpdateCheck = false;
            MintKitOptions.Instance.Serialize();
        }

        private void getIssuesBtn_Click(object sender, RoutedEventArgs e)
        {
            SharpBucketV1 bucket = new SharpBucketV1();
            bucket.OAuth2LeggedAuthentication("t9mm9F3tUkTmWQZfWH", "V8RTPZjjkpSGA96AfxdkwFaWDgym5ML5");
            var repositoryEndPoint = bucket.RepositoriesEndPoint("liquidize", "mintkit");
            var issuesResource = repositoryEndPoint.IssuesResource();
            // getting the list of all the issues of the repository
            var issues = issuesResource.ListIssues();
            for (int i = 0; i < issues.issues.Count; i++)
            {
                _issueInfos.Add(new SettingsIssueInfo()
                {
                    Title = issues.issues[i].title,
                    Description = issues.issues[i].content,
                    ReportedBy = issues.issues[i].reported_by != null ? issues.issues[i].reported_by.display_name : "anonymous",
                    Responsible = issues.issues[i].responsible != null ? issues.issues[i].responsible.display_name : "none",
                    Kind = issues.issues[i].metadata.kind,
                    Status = issues.issues[i].status,
                    Priority = issues.issues[i].priority,
                    DateCreated = issues.issues[i].utc_created_on,
                    DateUpdated = issues.issues[i].utc_last_updated
                });

            }
            issuesListView.ItemsSource = _issueInfos;
        }

        private void postIssueBtn_Click(object sender, RoutedEventArgs e)
        {
            var issueWindow = new NewIssueWindow();
            issueWindow.Show();
        }

        private void checkforupdatesBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateCallback(this, new EventArgs());
        }

        public void CheckForUpdates()
        {
            UpdateCallback(this, new EventArgs());
        }


        protected string GetMd5HashFromFile(string fileName)
        {
            FileStream file = new FileStream(fileName, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }


        private async void UpdateCallback(object sender, EventArgs eventArgs)
        {
            if (_updateInfo != null && _updateInfo.ChangeSet != null)
                _updateInfo.ChangeSet.Clear();


            _updateInfo = null;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    string json = await client.GetStringAsync("http://ffxivgoblin.com/mintkit/updates/version.json");
                    _updateInfo = JsonConvert.DeserializeObject<UpdateInfo>(json);
                    if (_updateInfo != null)
                    {
                        versionLbl.Content = _updateInfo.Version.ToString() + "-" + _updateInfo.VersionName;

                        updateInfoView.ItemsSource = _updateInfo.ChangeSet;

                        // Check if launcher needs updating.
                        if (File.Exists("MintKit.Loader.exe"))
                        {
                            string launcherVersion = GetMd5HashFromFile("MintKit.Loader.exe");
                            if (launcherVersion != _updateInfo.LauncherVersion)
                            {
                                UpdateLauncherAsync();
                            }
                        }
                        else
                        {
                            // Launcher doesn't exists, download new one.
                            UpdateLauncherAsync();
                        }

                        if (Version.Parse(_updateInfo.Version) > Assembly.GetExecutingAssembly().GetName().Version)
                        {
                            updateBtn.IsEnabled = true;

                            App.ClientWindowInstance.toastiControl.Message = "[" + DateTime.Now +
                                                                             "] New version avaliable " +
                                                                             versionLbl.Content +
                                                                             " goto settings to update your client!";
                        }
                        else
                        {
                            updateBtn.IsEnabled = false;
                        }
                    }
                }
                catch (HttpRequestException exception)
                {
                    if (MintKitOptions.Instance.DebugMode)
                    {
                        this.Log().Warn(exception.Message);
                    }
                    App.ClientWindowInstance.toastiControl.Message = "[" + DateTime.Now +
                                                                     "] Failed to retrieve version info an error occured.";

                }
            }
        }


        private async void UpdateLauncherAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    byte[] data =
                        await client.GetByteArrayAsync("http://ffxivgoblin.com/mintkit/updates/MintKit.Loader.exe");
                    File.WriteAllBytes("MintKit.Loader.exe", data);
                }
                catch (Exception ex)
                {
                    this.Log().Warn(ex.ToString);
                }
            }
        }


        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("MintKit.Loader.exe", "update");

            for (int i = 0; i < Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length; i++)
            {
                Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName)[i].Kill();
            }



            App.Current.Shutdown();
        }


    }
}
