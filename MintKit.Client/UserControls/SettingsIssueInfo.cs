﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintKit.Client.UserControls
{
    public class SettingsIssueInfo
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        public string Kind { get; set; }
        public string ReportedBy { get; set; }
        public string Responsible { get; set; }
        public string DateCreated { get; set; }
        public string DateUpdated { get; set; }
    }
}
