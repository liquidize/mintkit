﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MintKit.Api.Plugin;
using System.IO;
using System.Reflection;
using System.Windows;
using MintKit.Api.Ui.Controls;
using MintKit.Common.Logging;

namespace MintKit.Client.Core
{
    public class PluginManager
    {
        public static LinkedList<IMintKitPluginBase> Plugins = new LinkedList<IMintKitPluginBase>();
        internal static ILog log = Log.GetLoggerFor(typeof (PluginManager).FullName);
        
        public static IMintKitPluginBase GetPluginByName(string name)
        {
            LinkedListNode<IMintKitPluginBase> pluginNode = Plugins.First;
            while (pluginNode != null)
            {
                if (pluginNode.Value.PluginName == name)
                {
                    return pluginNode.Value;
                }

                pluginNode = pluginNode.Next;
            }
            return null;
        }

        public static int LoadPlugins()
        {
            int count = 0;

            log.Info("Loading Plugins...");
            if (Directory.Exists("Plugins"))
            {
                string[] pluginFiles = Directory.GetFiles("Plugins", "*.dll", SearchOption.AllDirectories);

                if (MintKitOptions.Instance.DebugMode)
                {
                    log.Debug("Plugin Files Found: {0}", pluginFiles.Length);
                }

                for (int i = 0; i < pluginFiles.Length; i++)
                {
                    try
                    {
                        Assembly pluginAssembly = Assembly.LoadFrom(pluginFiles[i]);

                        if (pluginAssembly == null)
                        {
                            log.Warn("Failed to load {0}", pluginFiles[i]);

                            continue;
                        }

                       // App.LoadingWindow.loadingLbl.Content = "Loading plugin from... " + pluginAssembly.FullName;

                        if (MintKitOptions.Instance.DebugMode)
                        {
                            log.Debug("Loaded Plugin Assembly: {0}", pluginAssembly.FullName);
                        }

                        Type[] pluginTypes = pluginAssembly.GetTypes();

                        for (int t = 0; t < pluginTypes.Length; t++)
                        {
                            Type[] pluginTypeInterfaces = pluginTypes[t].GetInterfaces();

                            for (int j = 0; j < pluginTypeInterfaces.Length; j++)
                            {
                                if (pluginTypeInterfaces[j] == typeof(IMintKitPluginBase))
                                {
                                    IMintKitPluginBase plugin = (IMintKitPluginBase)Activator.CreateInstance(pluginTypes[t]);
                                    plugin.Init();
                                    Plugins.AddLast(plugin);
                                   
                                    count += 1;
                                    
                                    if (MintKitOptions.Instance.DebugMode)
                                    {
                                        log.Debug("Plugin Loaded: {0}", plugin.PluginName);
                                    }
                                }
                            }

                        }

                    }
                    catch (Exception e)
                    {
                        if (MintKitOptions.Instance.DebugMode)
                        {
                            log.Error(e.ToString);
                        }
                        else
                        {
                            log.Error("Failed to load and initialize plugin from file: {0}", pluginFiles[i]);
                        }
                    }

                }


                // Load core plugins.
                Type[] coreTypes = Assembly.GetExecutingAssembly().GetTypes();
                for (int i = 0; i < coreTypes.Length; i++)
                {
                    Type[] corePluginTypeInterfaces = coreTypes[i].GetInterfaces();

                    for (int j = 0; j < corePluginTypeInterfaces.Length; j++)
                    {
                        if (corePluginTypeInterfaces[j] == typeof(IMintKitPluginBase))
                        {
                            IMintKitPluginBase plugin = (IMintKitPluginBase)Activator.CreateInstance(coreTypes[i]);
                            plugin.Init();
                            Plugins.AddLast(plugin);

                            count += 1;

                            if (MintKitOptions.Instance.DebugMode)
                            {
                                log.Debug("Plugin Loaded: {0}", plugin.PluginName);
                            }
                        }
                    }
                }

                log.Info("Loaded {0} Plugins.", count);

                App.ClientWindowInstance.Show();
                App.LoadingWindow.Hide();

            }
            else
            {
                if (MintKitOptions.Instance.DebugMode)
                    log.Debug("Plugins folder does not exists...creating it.");

                Directory.CreateDirectory("Plugins");

                // Attempt to reload plugins
                LoadPlugins();                
                
            }

            return count;
        }
    }
}
