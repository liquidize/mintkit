﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Threading;
using MintKit.Api.Ui.Controls;
using MintKit.Api.Plugin;

namespace MintKit.Client.Core.Plugins
{
    public class GameClientPlugin : IMintKitPluginBase
    {
        public MintKitPluginType PluginType { get { return MintKitPluginType.PrimaryTab;} }

        public string PluginName { get { return "RuneScape Client"; } }
        public string PluginCategory { get { return "Tools"; } }

        public string PluginAuthor
        {
            get { return "MintKit"; }
        }

        public string Version
        {
            get { return "1.1.0"; }
        }

        public string Description
        {
            get { return "Allows you to play RuneScape in JAVA using a Chrome \nbased game client in multiple languages."; }
        }

        public string PluginIcon { get { return "Resources\\images\\runescapelogoicon.png"; } }

        public UserControl Control
        {
            get { return new GameClient(); }
        }

        
        public void Init()
        {

        }

        public void DeInit()
        {

        }

        public void InstanceClosed(UserControl instance)
        {
            GameClient client = (GameClient) instance;
            if (client != null)
            {
                if (client.gameClientBrowser != null)
                client.gameClientBrowser.Dispose();
            }
        }

    }
}
