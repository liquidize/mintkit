﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CefSharp;
using CefSharp.WinForms;
using MintKit.Api.Ui.Controls;

namespace MintKit.Client.Core.Plugins
{
    /// <summary>
    /// Interaction logic for GameClient.xaml
    /// </summary>
    public partial class GameClient : UserControl
    {
        public ChromiumWebBrowser gameClientBrowser;

        public GameClient()
        {
            Cef.Initialize(new CefSettings() { IgnoreCertificateErrors = true });

            InitializeComponent();

            gameClientBrowser = new ChromiumWebBrowser("about:blank");
            gameClientBrowser.BrowserSettings.ApplicationCacheDisabled = false;
            gameClientBrowser.BrowserSettings.DatabasesDisabled = false;
            gameClientBrowser.BrowserSettings.FileAccessFromFileUrlsAllowed = true;
            gameClientBrowser.BrowserSettings.WebGlDisabled = false;
            gameClientBrowser.BrowserSettings.WebSecurityDisabled = true;

            winHost.Visibility = Visibility.Collapsed;


            languageChoiceBox.Items.Add("English");
            languageChoiceBox.Items.Add("German");
            languageChoiceBox.Items.Add("French");
            languageChoiceBox.Items.Add("Portuguese");

            languageChoiceBox.SelectionChanged += LanguageChoiceBoxOnSelectionChanged;

        }



        private void LanguageChoiceBoxOnSelectionChanged(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            if (languageChoiceBox.SelectedIndex == 0)
            {
                gameClientBrowser.Load("http://world100.runescape.com");
             
            }
            else if (languageChoiceBox.SelectedIndex == 1)
            {
                gameClientBrowser.Load("http://www.runescape.com/l=1/game.ws?autocreate=true&j=1");
             }
            else if (languageChoiceBox.SelectedIndex == 2)
            {
                gameClientBrowser.Load("http://www.runescape.com/l=2/game.ws?autocreate=true&j=1");

            }
            else if (languageChoiceBox.SelectedIndex == 3)
            {
                gameClientBrowser.Load("http://www.runescape.com/l=3/game.ws?autocreate=true&j=1");
            }
            winHost.Child = gameClientBrowser;
            winHost.Visibility = Visibility.Visible;
            languageLbl.Visibility = Visibility.Collapsed;
            languageChoiceBox.Visibility = Visibility.Collapsed;
        }
    }
}
