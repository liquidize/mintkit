﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro;
using MahApps.Metro.Controls;
using MintKit.Api;
using MintKit.Api.Ui;
using MintKit.Api.Ui.Controls;
using MintKit.Common.Logging;
using Parse;

namespace MintKit.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static MainWindow ClientWindowInstance { get; set; }
        internal static LoadingWindow LoadingWindow { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {

            // Initialize Logging Engine
            Log.InitializeWith<Log4NetLog>();
            
            // Initialize Global Settings.
            MintKitOptions.Deserialize();
            
            MintKitCoreApi.Initialize(new MintKitCoreImplementation());

            // Initialize Parse
            Parse.ParseClient.Initialize("ZWaAQc9V3SFPty5rPlHhO28CulaDFl7aimNCPxff", "VESU0csMb5SHaWCIM8VrMHSg1sj6gp0YY0SnDiBl");


            ClientWindowInstance = new MainWindow();
            ClientWindowInstance.Flyouts = new FlyoutsControl();
            // get the theme from the current application
            var theme = ThemeManager.DetectAppStyle(Application.Current);

            // now set the Emerald accent and dark theme
            ThemeManager.ChangeAppStyle(Application.Current,
                                        ThemeManager.GetAccent("Emerald"),
                                        ThemeManager.GetAppTheme("BaseDark"));

            base.OnStartup(e);
        }
    }
}
