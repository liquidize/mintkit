﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using MintKit.Client.Core;

namespace MintKit.Client
{
    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow
    {
        public LoadingWindow()
        {
            InitializeComponent();
        }

        private DispatcherTimer timer;

        private void ClientLoadWindow_Loaded(object sender, RoutedEventArgs e)
        {
            App.LoadingWindow = this;

            // we're purposely waiitng 3 seconds just so the actual window can load before we start loading plugins.
            timer = new DispatcherTimer(TimeSpan.FromSeconds(3), DispatcherPriority.Background, Callback,
                this.Dispatcher);
            timer.Start();

        }

        private void Callback(object sender, EventArgs eventArgs)
        {
            PluginManager.LoadPlugins();
            timer.Stop();
        }


        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
        }
    }
}
