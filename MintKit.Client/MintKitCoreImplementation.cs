﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MintKit.Api;

namespace MintKit.Client
{
    public class MintKitCoreImplementation : IMintKitCoreApi
    {
        public string GetCurrentAccent()
        {
            return MintKitOptions.Instance.AccentColor;
        }

        public string GetCurrentTheme()
        {
            return MintKitOptions.Instance.Theme;
        }

        public bool GetDebugMode()
        {
            return MintKitOptions.Instance.DebugMode;
        }

        public bool GetCacheMode()
        {
            return MintKitOptions.Instance.CacheImages;
        }

        public void IncludeResource(ResourceDictionary dictionary )
        {
            Application.Current.Resources.MergedDictionaries.Add(dictionary);
        }
    }
}
