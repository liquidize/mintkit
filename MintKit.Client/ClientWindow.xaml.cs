﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MintKit.Api.GrandExchange;
using MintKit.Api.Plugin;
using MintKit.Api.Ui.Controls;
using MintKit.Client.Core;
using MintKit.Client.UserControls;
using MintKit.Common.Logging;
using Parse;

namespace MintKit.Client
{
    /// <summary>
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private Flyout settingsFlyout;
        private SettingsControl settingsControl;

        public MainWindow()
        {
            InitializeComponent();

            this.MinWidth = 1028;
            this.MinHeight = 720;
            this.HomeStackPanel.CanVerticallyScroll = true;
            this.HomeStackPanel.Orientation = Orientation.Horizontal;
            this.Closed += OnClosed;
            this.SizeChanged += OnSizeChanged;
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
            if (settingsFlyout != null && settingsFlyout.IsOpen)
            {
                settingsFlyout.Width = this.Width;
                settingsFlyout.Height = this.Height;
            }
        }

        private void InitializeApis()
        {
            GrandExchangeApi.Initialize();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            foreach (IMintKitPluginBase plugin in PluginManager.Plugins)
            {
                this.Log().Info("DeInitializing Plugin: {0}", plugin.PluginName);
                try
                {
                    plugin.DeInit();
                }
                catch (Exception deInitException)
                {
                    this.Log().Warn("Could not properly deinitialize {0}.", plugin.PluginName);
                    this.Log().Error(deInitException.Message);
                }
            }
            base.OnClosing(e);
        }

        private void OnClosed(object sender, EventArgs eventArgs)
        {
            App.Current.Shutdown();
        }

        private void ClientWindow_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeApis();
            settingsFlyout = new Flyout();
            settingsFlyout.Position = Position.Right;
            settingsFlyout.Header = "Settings";
            this.Flyouts.Items.Add(settingsFlyout);

            settingsControl = new SettingsControl();
            settingsFlyout.Content = settingsControl;

            // Check for updates.
            settingsControl.CheckForUpdates();

            AnimatePrimaryTab();

            foreach (IMintKitPluginBase plugin in PluginManager.Plugins)
            {
                if (plugin.PluginType == MintKitPluginType.PrimaryTab)
                {
                    Tile pluginTile = new Tile();
                    pluginTile.Width = 100;
                    pluginTile.Height = 100;
                    pluginTile.ToolTip = plugin.PluginName;

                    if (plugin.PluginIcon != "")
                    {
                        if (File.Exists(plugin.PluginIcon))
                            pluginTile.Content = new Image()
                            {
                                Source = new BitmapImage(new Uri(plugin.PluginIcon, UriKind.RelativeOrAbsolute))
                            };
                    }

                    pluginTile.Click += (o, args) =>
                    {
                        Storyboard storyBoard = new Storyboard();
                        DoubleAnimation animation = new DoubleAnimation(100, 75, new Duration(TimeSpan.FromSeconds(0.5)));
                        animation.EasingFunction = new BounceEase() { Bounces = 2, Bounciness = 2, EasingMode = EasingMode.EaseOut };

                        Storyboard.SetTarget(animation, pluginTile);
                        Storyboard.SetTargetProperty(animation, new PropertyPath(FrameworkElement.HeightProperty));

                        storyBoard.Children.Add(animation);
                        storyBoard.Begin(pluginTile);

                        int instances = GetInstanceCount(plugin);
                        string header = plugin.PluginName;
                        if (instances > 0)
                            header = header + string.Format(" {0}", instances);

                        MetroTabItem pluginTabItem = new MetroTabItem()
                        {
                            Header = header,
                            Content = Activator.CreateInstance(plugin.Control.GetType())
                        };

                        pluginTabItem.CloseButtonEnabled = true;
                        pluginTabItem.Unloaded += (sender1, eventArgs) =>
                        {
                            plugin.InstanceClosed((UserControl)pluginTabItem.Content);
                        };

                        animation.To = 100;
                        animation.From = 75;
                        storyBoard.Begin(pluginTile);
                        primaryTabControl.Items.Add(pluginTabItem);


                    };
                    HomeStackPanel.Children.Add(pluginTile);




                }
                else if (plugin.PluginType == MintKitPluginType.SecondaryTab)
                {
                    MenuItem pluginLabel = new MenuItem();
                    pluginLabel.Header = plugin.PluginName;
                    pluginLabel.Click += (o, args) =>
                    {
                        
                        MetroTabItem item = new MetroTabItem();
                        item.CloseButtonEnabled = true;
                        item.Header = new Image()
                        {
                            Source = new BitmapImage(new Uri(plugin.PluginIcon, UriKind.RelativeOrAbsolute)),
                            Stretch = Stretch.UniformToFill,
                            Width = 32,
                            Height = 32,
                            ToolTip = plugin.PluginName
                        };

                        item.Content = plugin.Control;
                        item.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                        item.VerticalContentAlignment = VerticalAlignment.Stretch;
                        secondaryTabControl.Items.Add(item);
                        pluginLabel.IsEnabled = false;
                        AnimatePrimaryTab();

                        item.Unloaded += (sender1, eventArgs) =>
                        {
                            pluginLabel.IsEnabled = true;
                            AnimatePrimaryTab();
                        };
                    };
                    for (int i = 0; i < ClientMenuBar.Items.Count; i++)
                    {
                        MenuItem mainItem = ClientMenuBar.Items[i] as MenuItem;
                        if ((string)mainItem.Header == plugin.PluginCategory)
                        {
                            mainItem.Items.Add(pluginLabel);
                            break;
                        }
                        if (i == ClientMenuBar.Items.Count - 1)
                        {
                            ClientMenuBar.Items.Add(pluginLabel);
                            break;
                        }
                    }
                }
                else if (plugin.PluginType == MintKitPluginType.TeritaryTab)
                {
                    MenuItem pluginLabel = new MenuItem();
                    pluginLabel.Header = plugin.PluginName;
                    pluginLabel.Click += (o, args) =>
                    {
                        MetroTabItem item = new MetroTabItem();
                        item.CloseButtonEnabled = true;
                        item.Header = new Image()
                        {
                            Source = new BitmapImage(new Uri(plugin.PluginIcon, UriKind.RelativeOrAbsolute)),
                            Stretch = Stretch.UniformToFill,
                            Width = 32,
                            Height = 32,
                            ToolTip = plugin.PluginName
                        };

                        item.Content = plugin.Control;
                        item.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                        item.VerticalContentAlignment = VerticalAlignment.Stretch;
                        teritaryTabControl.Items.Add(item);
                        pluginLabel.IsEnabled = false;
                        AnimatePrimaryTab();
                        item.Unloaded += (sender1, eventArgs) =>
                        {
                            AnimatePrimaryTab();
                            pluginLabel.IsEnabled = true;
                        };
                    };
                    for (int i = 0; i < ClientMenuBar.Items.Count; i++)
                    {
                        MenuItem mainItem = ClientMenuBar.Items[i] as MenuItem;
                        if ((string)mainItem.Header == plugin.PluginCategory)
                        {
                            mainItem.Items.Add(pluginLabel);
                            break;
                        }
                        if (i == ClientMenuBar.Items.Count - 1)
                        {
                            ClientMenuBar.Items.Add(pluginLabel);
                            break;
                        }
                    }
                }

                else if (plugin.PluginType == MintKitPluginType.Window)
                {
                    MenuItem pluginLabel = new MenuItem();
                    pluginLabel.Header = plugin.PluginName;
                    pluginLabel.Click += (o, args) =>
                    {
                        MetroWindow _window = new MetroWindow();
                        _window.Title = plugin.PluginName;
                        _window.ResizeMode = ResizeMode.CanResizeWithGrip;
                        _window.SizeToContent = SizeToContent.WidthAndHeight;
                        _window.Icon = new BitmapImage(new Uri(plugin.PluginIcon, UriKind.RelativeOrAbsolute));
                        _window.Content = plugin.Control;
                        _window.Show();
                        pluginLabel.IsEnabled = false;

                        _window.Closed += (sender1, eventArgs) =>
                        {
                            pluginLabel.IsEnabled = true;
                        };
                    };

                    for (int i = 0; i < ClientMenuBar.Items.Count; i++)
                    {
                        MenuItem mainItem = ClientMenuBar.Items[i] as MenuItem;
                        if ((string)mainItem.Header == plugin.PluginCategory)
                        {
                            mainItem.Items.Add(pluginLabel);
                            break;
                        }
                        if (i == ClientMenuBar.Items.Count - 1)
                        {
                            ClientMenuBar.Items.Add(pluginLabel);
                            break;
                        }
                    }
                }
            }
        }



        private void settingsButton_Click(object sender, RoutedEventArgs e)
        {
            settingsFlyout.Width = this.Width;
            settingsFlyout.Height = this.Height;
            settingsFlyout.IsOpen = !settingsFlyout.IsOpen;
            settingsFlyout.CloseButtonVisibility = Visibility.Hidden;
            settingsFlyout.Theme = FlyoutTheme.Adapt;
            primaryTabControl.Visibility = primaryTabControl.IsVisible ? Visibility.Hidden : Visibility.Visible;

        }


        private int GetInstanceCount(IMintKitPluginBase plugin)
        {
            int instances = 0;
            for (int i = 0; i < primaryTabControl.Items.Count; i++)
            {
                MetroTabItem tab = primaryTabControl.Items[i] as MetroTabItem;
                if (tab != null && tab.Content.GetType() == plugin.Control.GetType())
                {
                    instances++;
                }
            }
            return instances;
        }

        private void AnimatePrimaryTab()
        {
            if (secondaryTabControl.Items.Count > 0 || teritaryTabControl.Items.Count > 0)
            {
                primaryTabControl.Margin = new Thickness(0, 0, 310, 30);
            }
            else if (secondaryTabControl.Items.Count == 0 && teritaryTabControl.Items.Count == 0)
            {
                primaryTabControl.Margin = new Thickness(0, 0, 0, 30);
            }
        }

        private async void loginButton_Click(object sender, RoutedEventArgs e)
        {    try
            {
                toastiControl.Message = "[" + DateTime.Now + "] Community System coming soon! Look forward to it!";
              
            }
            catch (Exception xe)
            {
                throw xe;
            }
        }
    }
}
