﻿using System.Collections.Generic;

namespace MintKit.Client.SharpBucket.V1.Pocos{
    public class Privileges{
        public Dictionary<string, string> teams { get; set; }
    }
}
