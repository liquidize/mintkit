﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MintKit.Api
{
    /// <summary>
    /// Core API Class used to obtain information regarding the client, plugins are meant to be self contained and secure so using these allows plugins to obtain relavent information,
    /// about the user and the client, without being able to overwrite or set it.
    /// </summary>
   public class MintKitCoreApi
   {
        /// <summary>
        /// The instance of IMintKitCoreApi that implements these functions.
        /// </summary>
       private static IMintKitCoreApi implementation;

        /// <summary>
        /// Initialize the CoreAPI called on the main client, does not need to be overwritten.
        /// </summary>
        /// <param name="impl"></param>
       public static void Initialize(IMintKitCoreApi impl)
       {
           if (implementation == null) // check if null to not allow plugins to overwrite this.
           implementation = impl;
       }


        /// <summary>
        /// Gets the current accent style.
        /// </summary>
        /// <returns></returns>
       public static string GetCurrentAccent()
       {
           return implementation.GetCurrentAccent();
       }

        /// <summary>
        /// Gets the current theme.
        /// </summary>
        /// <returns></returns>
       public static string GetCurrentTheme()
       {
           return implementation.GetCurrentTheme();
       }

        /// <summary>
        /// Gets if the cleint is in debug mode.
        /// </summary>
        /// <returns></returns>
       public static bool GetDebugMode()
       {
           return implementation.GetDebugMode();
       }

        /// <summary>
        /// Gets if the client is allowing caching.
        /// </summary>
        /// <returns></returns>
       public static bool GetCacheMode()
       {
           return implementation.GetCacheMode();
       }

        /// <summary>
        /// Includes a resource dictionary in the main client.
        /// </summary>
        /// <param name="dictionary"></param>
       public static void IncludeResource(ResourceDictionary dictionary)
       {
           implementation.IncludeResource(dictionary);
       }

   }
}
