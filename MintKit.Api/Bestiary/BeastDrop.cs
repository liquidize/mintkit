﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintKit.Api.Bestiary
{
    /// <summary>
    /// Item Rarity enum. Defines how rare items are.
    /// </summary>
    public enum ItemRarity
    {
        Always = 0,
        Common = 1,
        Uncommon = 2,
        Rare = 3,
        VeryRare = 4,
        Varies = 5
    }

    /// <summary>
    /// Beast Drop Class. Stores information on loot given by NPCs and Monsters.
    /// </summary>
    public class BeastDrop
    {
        /// <summary>
        /// Name of the item.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// ID of the item.
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Notes/remarks regarding the drop.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Rarity of the drop.
        /// </summary>
        public ItemRarity Rarity { get; set; }

        /// <summary>
        /// Amount dropped.
        /// </summary>
        /// <remarks>This is a string instead of an int, preferably formatted as MIN-MAX (e.g: 10-20). Due to NPCs giving varied amount of loot.</remarks>
        public string Amount { get; set; }
    }
}
