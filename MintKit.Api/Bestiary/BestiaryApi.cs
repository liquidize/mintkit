﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MintKit.Common.Logging;
using Newtonsoft.Json;

namespace MintKit.Api.Bestiary
{
    /// <summary>
    /// Beast API. Get information regarding beast via Jagex's offical Bestiary API.
    /// </summary>
    public class BestiaryApi
    {
        internal static ILog _log = Log.GetLoggerFor(typeof (BestiaryApi).FullName);

        /// <summary>
        /// Get a formatted URL to a JSON object containing names and ids of beast containing the given terms
        /// </summary>
        /// <param name="terms">terms to search with given as "term1+term2+term3..." </param>
        /// <returns></returns>
        public static string BEASTIARY_NAME_BY_TERM_URL(string terms)
        {
            return string.Format("http://services.runescape.com/m=itemdb_rs/bestiary/beastSearch.json?term={0}",terms);
        }

        /// <summary>
        /// Get information regarding a beast given its numeric ID.
        /// </summary>
        /// <param name="id">Id of the beast to search for.</param>
        /// <returns></returns>
        public static string BESTIARY_BEAST_BY_ID_URL(int id)
        {
            return string.Format("http://services.runescape.com/m=itemdb_rs/bestiary/beastData.json?beastid={0}", id);
        }

        /// <summary>
        /// Retrieves a list of BeastNameId instances given search terms.
        /// </summary>
        /// <param name="terms">terms to search for given as "term1+term2+term3"</param>
        /// <returns>Null if an error ocurrs or the List of BeastNameId</returns>
        public async static Task<List<BeastNameId>> GetBeastsByTermAsync(string terms)
        {
            List<BeastNameId> beastList = null;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    string json = await client.GetStringAsync(BEASTIARY_NAME_BY_TERM_URL(terms));
                    beastList = JsonConvert.DeserializeObject<List<BeastNameId>>(json);
                    return beastList;
                }
                catch (HttpRequestException httpException)
                {
                    if (MintKitCoreApi.GetDebugMode())
                       _log.Error(httpException.Message);
                    return beastList;
                }
                catch (JsonException exception)
                {
                    if (MintKitCoreApi.GetDebugMode())
                        _log.Error(exception.Message);
                    return beastList;
                }
            }
        }

        /// <summary>
        /// Get an Instance of a Beast given its ID.
        /// </summary>
        /// <param name="id">numeric ID of the beast.</param>
        /// <returns>instance of a beast, or null if an error occurrs.</returns>
        public async static Task<Beast> GetBeastById(int id)
        {
            Beast beast = null;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    string json = await client.GetStringAsync(BESTIARY_BEAST_BY_ID_URL(id));
                    beast = JsonConvert.DeserializeObject<Beast>(json);
                    return beast;
                }
                catch (HttpRequestException httpException)
                {
                    if (MintKitCoreApi.GetDebugMode())
                        _log.Error(httpException.Message);
                    return beast;
                }
                catch (JsonException exception)
                {
                    if (MintKitCoreApi.GetDebugMode())
                        _log.Error(exception.Message);
                    return beast;
                }
            }
        } 
    }
}
