﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MintKit.Api.Bestiary
{
    /// <summary>
    /// Animation ID for a Beast Instance.
    /// </summary>
    public class BeastAnimations
    {
        /// <summary>
        /// Death animation ID.
        /// </summary>
        [JsonProperty("death")]
        public int Death { get; set; }

        /// <summary>
        /// Attack animation ID.
        /// </summary>
        [JsonProperty("attack")]
        public int Attack { get; set; }
    }

    public class Beast
    {
        /// <summary>
        /// The magic level of the beast/npc.
        /// </summary>
        [JsonProperty("magic")]
        public int Magic { get; set; }

        /// <summary>
        /// Slayer category of the beast/npc.
        /// </summary>
        [JsonProperty("slayercat")]
        public string SlayerCat { get; set; }

        /// <summary>
        /// Defence level of the beast/npc.
        /// </summary>
        [JsonProperty("defence")]
        public int Defence { get; set; }

        /// <summary>
        /// Combat level of the beast/npc.
        /// </summary>
        [JsonProperty("level")]
        public int Level { get; set; }

        /// <summary>
        /// Examine text of the beast/npc.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Areas within the game, this beast is found.
        /// </summary>
        [JsonProperty("areas")]
        public List<string> Areas { get; set; }

        /// <summary>
        /// Is this beast/npc poisonous?
        /// </summary>
        [JsonProperty("poisonous")]
        public bool Poisonous { get; set; }

        /// <summary>
        /// This beasts/npcs weakness.
        /// </summary>
        [JsonProperty("weakness")]
        public string Weakness { get; set; }

        /// <summary>
        /// Size of the beast/npc?
        /// </summary>
        /// <remarks>Not entirely sure what this is used for.</remarks>
        [JsonProperty("size")]
        public int Size { get; set; }

        /// <summary>
        /// Ranged level of the beast/npc.
        /// </summary>
        [JsonProperty("ranged")]
        public int Ranged { get; set; }

        /// <summary>
        /// Attack level of the beast/npc.
        /// </summary>
        [JsonProperty("attack")]
        public int Attack { get; set; }

        /// <summary>
        /// Is the beast/npc members only?
        /// </summary>
        [JsonProperty("members")]
        public bool Members { get; set; }

        /// <summary>
        /// Animation IDs of the beast.
        /// </summary>
        [JsonProperty("animations")]
        public BeastAnimations Animations { get; set; }

        /// <summary>
        /// Name of the beast/npc.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Base XP given by the beast/npc.
        /// </summary>
        [JsonProperty("xp")]
        public string Experience { get; set; }

        /// <summary>
        /// How much HP the beast/npc has.
        /// </summary>
        [JsonProperty("lifepoints")]
        public int LifePoints { get; set; }

        /// <summary>
        /// ID of the beast/npc.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Is the beast aggressive?
        /// </summary>
        [JsonProperty("aggressive")]
        public bool Aggressive { get; set; }

        /// <summary>
        /// Is the beast attackable?
        /// </summary>
        [JsonProperty("attackable")]
        public bool Attackable { get; set; }

        /// <summary>
        /// Internal Drop table of the beast, produced by MintKit internally and not an official part of the Bestiary API.
        /// </summary>
        /// <remarks>This will always be null for now, it is not returned by the Jagex Bestiary API, but rather created by MintKit in some instances.</remarks>
        public List<BeastDrop> DropTable { get; set; } 
    }
}
