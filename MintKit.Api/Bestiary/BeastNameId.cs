﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace MintKit.Api.Bestiary
{

    /// <summary>
    /// Beast Name/ID instace. Contains information regarding the name of a beast and its corresponding ID. Used in GetBeastsByTermAsync
    /// </summary>
    public class BeastNameId
    {
        /// <summary>
        /// Name of the beast.
        /// </summary>
        [JsonProperty("label")]
        public string Name { get; set; }

        /// <summary>
        /// Id of the beast.
        /// </summary>
        [JsonProperty("value")]
        public int Id { get; set; }
    }
}
