﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintKit.Api.Plugin
{
    /// <summary>
    /// Enum of plugin types.
    /// </summary>
   public enum MintKitPluginType
    {
        PrimaryTab = 0,
        SecondaryTab = 1,
        TeritaryTab = 2,
        Window = 3
    }
}
