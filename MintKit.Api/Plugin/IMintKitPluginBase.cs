﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MintKit.Api.Plugin
{
    public interface IMintKitPluginBase
    {
        /// <summary>
        // Get the the type of plugin.
        // PrimaryTab = for larger screen spaced plugins, like the game client.
        // SecondaryTab/Teritary = for smaller, plugins that allow you to continue to use primary tab plugins. Smaller screen space.
        // Window = plugins contained in their own window, these plugins steal focus from game and main client.
        /// </summary>
        MintKitPluginType PluginType { get; }

        /// <summary>
        /// Name of the plugin. Must be unique.
        /// </summary>
        string PluginName { get;}

        /// <summary>
        /// Path to the plugin icon.
        /// </summary>
        string PluginIcon { get; }

        /// <summary>
        /// Category of the plugin. When PluginType is Window or SecondaryTab/TeritaryTab this is used too determine where in the menu it goes.
        /// </summary>
        string PluginCategory { get; }

        /// <summary>
        /// Author of the plugin.
        /// </summary>
        string PluginAuthor { get; }

        /// <summary>
        /// Plugin Version.
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Description of the plugin.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// The GUI control of the plugin.
        /// </summary>
        UserControl Control { get; }

        /// <summary>
        /// Called when the plugin is Initialized (this occurrs when the plugin is loaded.)
        /// </summary>
        void Init();

        /// <summary>
        /// Called when the plugin is deinitialized (this occurrs when the client is closed).
        /// </summary>
        void DeInit();

        /// <summary>
        /// Called when an instance of this plugin is closed, this is used for PrimaryTab plugins, and is generally used to do custom disposing.
        /// e.g: CEFSharp does not dispose properly unless Dispose is called on the webcontrol, so we call that here when the tab of the instance is closed.
        /// </summary>
        /// <param name="instance">instance of the control being closed</param>
        void InstanceClosed(UserControl instance);

    }
}
