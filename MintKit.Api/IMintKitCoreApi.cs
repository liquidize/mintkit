﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MintKit.Api
{
    /// <summary>
    /// Interface for the Core API this is inherited in the client by a class that acts as the intructor. <see cref="MintKitCoreApi"/>
    /// </summary>
    public interface IMintKitCoreApi
    {
        /// <summary>
        /// Get the current accent style. Of the client
        /// </summary>
        /// <returns></returns>
        string GetCurrentAccent();
        
        /// <summary>
        /// Get the current theme of the client.
        /// </summary>
        /// <returns></returns>
        string GetCurrentTheme();

        /// <summary>
        /// Is the client in debug mode?
        /// </summary>
        /// <returns></returns>
        bool GetDebugMode();

        /// <summary>
        /// Is the client allowing cacheing?
        /// </summary>
        /// <returns></returns>
        bool GetCacheMode();
        
        /// <summary>
        /// Include a Resource (dictionary) in the main clients, main dictionary. merging it. Allowing for custom themes/etc to be used.
        /// </summary>
        /// <param name="dictionary"></param>
        void IncludeResource(ResourceDictionary dictionary);
    }
}
