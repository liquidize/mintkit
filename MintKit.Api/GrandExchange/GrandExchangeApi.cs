﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Formatters;
using System.Windows;
using MintKit.Common.Logging;
using Newtonsoft.Json;

namespace MintKit.Api.GrandExchange
{

    /// <summary>
    /// Grand Exchange API. Get Information Regarding Items and Item Categories.
    /// </summary>
    public class GrandExchangeApi
    {

        /// <summary>
        /// Dictionary of Category names, keyed by their id.
        /// </summary>
        public static Dictionary<int, string> GeCategories = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary of ItemIds keyed by their name.
        /// </summary>
        public static Dictionary<string, int> ItemInformation = new Dictionary<string, int>();

        /// <summary>
        /// Initialize the GrandExchange Api. This adds all the Category Id's and Names to the dictionary as well as Item Names/Id's to the dictionary.
        /// This only needs to be called once, and is done so by the core client.
        /// </summary>
        public static void Initialize()
        {
            // We reinitialize the Dictionary in case some dipshit decides they want to call it in their plugin anyway, so to not have duplicates.
            GeCategories = new Dictionary<int, string>();

            // Add all the category names and IDs.
            GeCategories.Add(0, "Miscellaneous");
            GeCategories.Add(1, "Ammo");
            GeCategories.Add(2, "Arrows");
            GeCategories.Add(3, "Bolts");
            GeCategories.Add(4, "Construction Materials");
            GeCategories.Add(5, "Construction Projects");
            GeCategories.Add(6, "Cooking Ingredients");
            GeCategories.Add(7, "Constumes");
            GeCategories.Add(8, "Crafting Materials");
            GeCategories.Add(9, "Familiars");
            GeCategories.Add(10, "Farming Produce");
            GeCategories.Add(11, "Fletching Materials");
            GeCategories.Add(12, "Food and Drink");
            GeCategories.Add(13, "Herblore materials");
            GeCategories.Add(14, "Hunting equipment");
            GeCategories.Add(15, "Hunting produce");
            GeCategories.Add(16, "Jewellery");
            GeCategories.Add(17, "Mage armour");
            GeCategories.Add(18, "Mage weapons");
            GeCategories.Add(19, "Melee armour - low level");
            GeCategories.Add(20, "Melee armour - mid level");
            GeCategories.Add(21, "Melee armour - high level");
            GeCategories.Add(22, "Melee weapons - low level");
            GeCategories.Add(23, "Melee weapons - mid level");
            GeCategories.Add(24, "Melee weapons - high level");
            GeCategories.Add(25, "Mining and smithing");
            GeCategories.Add(26, "Potions");
            GeCategories.Add(27, "Prayer armour");
            GeCategories.Add(28, "Prayer materials");
            GeCategories.Add(29, "Range armour");
            GeCategories.Add(30, "Range weapons");
            GeCategories.Add(31, "Runecrafting");
            GeCategories.Add(32, "Runes, Spells and Teleports");
            GeCategories.Add(33, "Seeds");
            GeCategories.Add(34, "Summmoning scrolls");
            GeCategories.Add(35, "Tools and containers");
            GeCategories.Add(36, "Woodcutting product");
            GeCategories.Add(37, "Pocket items");

            // Load Item Information
            // Again reinitialize the dictionary, incase a bigger dipshit wants too do this method anyway.
            ItemInformation = new Dictionary<string, int>();

            // Does the ItemDB information exists?
            if (File.Exists("Resources/data/itemdb.json"))
            {
                // Deserialize it.
                ItemInformation =
                    JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText("Resources/data/itemdb.json"));
            }
            else
            {
                Log.GetLoggerFor(typeof(GrandExchangeApi).FullName).Warn("Item Database File does not exists.");
            }

        }

        /// <summary>
        /// Return an item name from a given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetItemNameFromId(int id)
        {
            var item = ItemInformation.FirstOrDefault(itm => itm.Value == id);
            if (item.Value != 0 && string.IsNullOrEmpty(item.Key) != true)
                return item.Key;
            return "";
        }

        /// <summary>
        /// Return an array of item names, that contain the given string.
        /// </summary>
        /// <param name="names"></param>
        /// <returns></returns>
        public static string[] GetItemNamesFromString(string names)
        {
            var items = ItemInformation.Where(n => CultureInfo.CurrentCulture.CompareInfo.IndexOf(n.Key, names, CompareOptions.IgnoreCase) >= 0).Select(key => key.Key).ToArray();
            return items;
        }

        /// <summary>
        /// Gets the Category ID from a given name, returns -1 if no valid category id found.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetCategoryFromName(string name)
        {
            if (GeCategories == null)
                return -1;
            var id = GeCategories.FirstOrDefault(n => n.Value == name);
            if (string.IsNullOrEmpty(id.Value))
                return -1;

            return id.Key;

        }

        /// <summary>
        /// Get the name of a category given its ID. Returns null if not a valid ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetCategoryNameFromId(int id)
        {
            if (GeCategories == null)
                return "";
            var kvp = GeCategories.FirstOrDefault(n => n.Key == id);
            if (kvp.Key == 0 && string.IsNullOrEmpty(kvp.Value)) // since 0 is an actual category and is the default, check if value is also null
                return "";

            return kvp.Value;
        }

        /// <summary>
        /// Retrieve a formatted catalogue url string from the given information.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="alpha"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        internal static string CATALOGUE_ITEMS_URL(object category, object alpha, int page)
        {
            return
                string.Format(
                    "http://services.runescape.com/m=itemdb_rs/api/catalogue/items.json?category={0}&alpha={1}&page={2}", category, alpha, page);
        }

        /// <summary>
        /// Retrieve a formatted category information url given the category name or id.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        internal static string CATEGORY_INFORMATION_URL(object category)
        {
            return string.Format("http://services.runescape.com/m=itemdb_rs/api/catalogue/category.json?category={0}",
                category);
        }

        /// <summary>
        /// Retrieve a formatted Item details url given the item id.
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        internal static string ITEM_DETAILS_URL(int itemId)
        {
            return string.Format("http://services.runescape.com/m=itemdb_rs/api/catalogue/detail.json?item={0}", itemId);
        }

        /// <summary>
        /// Obtains the number of items in the given category for every starting letter.
        /// </summary>
        /// <param name="category">name of category or id</param>
        /// <returns></returns>
        public async static Task<List<Alpha>> GetCategoryInformationDetailsAsync(object category)
        {
            int realCategory = -1;
            if (category.GetType() == typeof(string))
                realCategory = GetCategoryFromName((string)category);
            if (category.GetType() == typeof(int))
                realCategory = (int)category;
            if (category.GetType() != typeof(int) && realCategory == -1)
                return null;
            if (realCategory < 0 || realCategory > 37)
                return null;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    var result = await client.GetStringAsync(CATEGORY_INFORMATION_URL(realCategory));
                    var tempAlphaList = JsonConvert.DeserializeObject<AlphaRoot>(result);

                    if (tempAlphaList != null)
                        return tempAlphaList.alpha;

                    return null;
                }
                catch (HttpRequestException httpEx)
                {
                    Log.GetLoggerFor(typeof(GrandExchangeApi).FullName)
                        .Warn("Failed to retrieve Category Information for category {0}.", category);
                    return null;
                }
                catch (JsonException jsonEx)
                {
                    Log.GetLoggerFor(typeof(GrandExchangeApi).FullName)
                        .Warn("Failed to deserialize Category Information for category {0}.", category);
                    return null;
                }
            }
        }

        /// <summary>
        /// Retrieve information on a given Item, using the Item Id
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static async Task<Item> GetItemDetailsAsync(int itemId)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string result = await client.GetStringAsync(ITEM_DETAILS_URL(itemId));
                    Item item = JsonConvert.DeserializeObject<ItemRoot>(result).Item;
                    if (item != null)
                    {
                        return item;
                    }
                    return null;
                }
            }
            catch (HttpRequestException httpEx)
            {
                Log.GetLoggerFor(typeof(GrandExchangeApi).FullName)
                    .Warn("Failed to retrieve Item Information for item {0}.", itemId);
                return null;
            }
            catch (JsonException jsonEx)
            {
                Log.GetLoggerFor(typeof(GrandExchangeApi).FullName)
                    .Warn("Failed to retrieve Item Information for item {0}.", itemId);
                return null;
            }
        }


        /// <summary>
        /// Obtains all items from a given GE Category, and Alpha string. You are able to specify a limit.
        /// This is only ran till there are no longer any items to return, but its best to specify a limit if you know the limit.
        /// By default no limit is set (limit = null). However this function doesn't wait, and calling this for large data pulls may result
        /// in Jagex banning your IP from using their API.
        ///
        /// </summary>
        /// <param name="category">category string or id</param>
        /// <param name="alpha">alpha-numeric string to search with, the more specific you are the less results. use %numbershere for number searches.</param>
        /// <param name="limit">Amount of items to return, null = no limit</param>
        /// <returns>Class containing Total amount of items in the given category, and List of items retrieved.</returns>
        public static async Task<CategoryPriceDetails> GetCategoryPriceDetailsAsync(object category, string alpha, object limit = null)
        {
            int realCategory = -1;
            if (category.GetType() == typeof(string))
                realCategory = GetCategoryFromName((string)category);
            if (category.GetType() == typeof(int))
                realCategory = (int)category;
            if (category.GetType() != typeof(int) && realCategory == -1)
                return null;

            if (limit == null)
            {
                limit = 10000;
            }

            int total = 0;
            List<Item> items = new List<Item>();
            int pages = (int)Math.Ceiling((double)((int)limit / 12));

            for (int i = 1; i <= pages; i++)
            {
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        var result = await client.GetStringAsync(CATALOGUE_ITEMS_URL(realCategory, alpha, i));
                        var tempCategoryPriceDetails = JsonConvert.DeserializeObject<CategoryPriceDetails>(result);

                        // Check if this is null, realistically this probably wont ever be reached, as if an HTTP Error occurrs an Exception is thrown, as is a JSON Ex.
                        if (tempCategoryPriceDetails == null)
                        {
                            if (items.Count > 0)
                            {
                                return new CategoryPriceDetails() { Items = items, Total = total };
                            }
                            return null;
                        }

                        // Set the total amount of items in this category.
                        total = tempCategoryPriceDetails.Total;

                        // If this instance of CategoryPriceDetails has 0 items, we've reached the last page, so break and return.
                        if (tempCategoryPriceDetails.Items.Count == 0)
                            break;

                        // Add items from this instance to the main instance.
                        items.AddRange(tempCategoryPriceDetails.Items);
                    }
                    catch (HttpRequestException httpEx)
                    {
                        if (items.Count > 0)
                        {
                            Log.GetLoggerFor(typeof(GrandExchangeApi).FullName).Warn("GetCategoryPriceDetails completed with an error. Returning {0} items.", items.Count);
                            return new CategoryPriceDetails() { Items = items, Total = total };
                        }
                        Log.GetLoggerFor(typeof(GrandExchangeApi).FullName).Warn("GetCategoryPriceDetails completed with an error. Returning {0} items.", items.Count);
                        Log.GetLoggerFor(typeof(GrandExchangeApi).FullName).Warn(httpEx.Message);
                        return null;


                    }
                    catch (JsonException jsonEx)
                    {
                        if (items.Count > 0)
                        {
                            Log.GetLoggerFor(typeof(GrandExchangeApi).FullName).Warn("GetCategoryPriceDetails completed with an error. Returning {0} items.", items.Count);
                            return new CategoryPriceDetails() { Items = items, Total = total };
                        }
                        Log.GetLoggerFor(typeof(GrandExchangeApi).FullName).Warn("GetCategoryPriceDetails completed with an error. Returning {0} items.", items.Count);
                        Log.GetLoggerFor(typeof(GrandExchangeApi).FullName).Warn(jsonEx.Message);
                        return null;
                    }
                }
            }

            // If Items list is larger than the wanted limit, trimm it.
            if (items.Count > (int)limit)
            {
                while (items.Count > (int)limit)
                {
                    items.RemoveAt(items.Count - 1);
                }
            }
            return new CategoryPriceDetails() { Items = items, Total = total };
        }
        
    }
}
