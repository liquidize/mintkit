﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MintKit.Api.GrandExchange
{
    /// <summary>
    /// Current Price Information on an Item.
    /// </summary>
    public class Current
    {
        /// <summary>
        /// Current Price Trend.
        /// </summary>
        [JsonProperty("trend")]
        public string Trend { get; set; }

        /// <summary>
        /// Current Item Price.
        /// </summary>
        [JsonProperty("price")]
        public object Price { get; set; }
    }

    /// <summary>
    /// Today Price Information on an Item.
    /// </summary>
    /// <remarks>Not entirely sure what this is, im guessing its the average of today?</remarks>
    public class Today
    {
        /// <summary>
        /// Today Price Trend.
        /// </summary>
        [JsonProperty("trend")]
        public string Trend { get; set; }

        /// <summary>
        /// Today Price.
        /// </summary>
        [JsonProperty("price")]
        public object Price { get; set; }
    }

    /// <summary>
    /// 30 Day Average Price Information for an Item.
    /// </summary>
    public class Day30
    {
        /// <summary>
        /// 30 Day Price Trend.
        /// </summary>
        [JsonProperty("trend")]
        public string Trend { get; set; }

        /// <summary>
        /// 30 Day Average Change.
        /// </summary>
        [JsonProperty("change")]
        public string Change { get; set; }
    }

    /// <summary>
    /// 90 Day Average Price Info.
    /// </summary>
    public class Day90
    {
        /// <summary>
        /// 90 Day Price Trend.
        /// </summary>
        [JsonProperty("trend")]
        public string Trend { get; set; }

        /// <summary>
        /// 90 Day Average Change.
        /// </summary>
        [JsonProperty("change")]
        public string Change { get; set; }
    }

    /// <summary>
    /// 180 Day average price information.
    /// </summary>
    public class Day180
    {
        /// <summary>
        /// 180 Day Price Trend.
        /// </summary>
        [JsonProperty("trend")]
        public string Trend { get; set; }

        /// <summary>
        /// 180 Day Average Change.
        /// </summary>
        [JsonProperty("change")]
        public string Change { get; set; }
    }

    /// <summary>
    /// Instance of an Item, containing information about an Item found on the grand exchange.
    /// </summary>
    public class Item
    {
        /// <summary>
        /// URL to the item icon.
        /// </summary>
        [JsonProperty("icon")]
        public string Icon { get; set; }
        
        /// <summary>
        /// URL to the large item icon.
        /// </summary>
        [JsonProperty("icon_large")]
        public string IconLarge { get; set; }

        /// <summary>
        /// Id of the Item.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Type of item.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Type Icon.
        /// </summary>
        [JsonProperty("typeIcon")]
        public string TypeIcon { get; set; }

        /// <summary>
        /// Name of the Item.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Examine text of the item.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Current Price Information for the Item.
        /// </summary>
        [JsonProperty("current")]
        public Current Current { get; set; }

        /// <summary>
        /// Today Price Information.
        /// </summary>
        [JsonProperty("today")]
        public Today Today { get; set; }

        /// <summary>
        /// 30 Day Average Information.
        /// </summary>
        [JsonProperty("day30")]
        public Day30 Day30 { get; set; }

        /// <summary>
        /// 90 Day Average Information.
        /// </summary>
        [JsonProperty("day90")]
        public Day90 Day90 { get; set; }


        /// <summary>
        /// 180 Day price information.
        /// </summary>
        [JsonProperty("day180")]
        public Day180 Day180 { get; set; }

        /// <summary>
        /// Is the Item Members?
        /// </summary>
        [JsonProperty("members")]
        public string Members { get; set; }
    }


    /// <summary>
    /// Information regarding items in a specific category.
    /// </summary>
    public class CategoryPriceDetails
    {
        /// <summary>
        /// Total amount of items returned from the search.
        /// </summary>
        [JsonProperty("total")]
        public int Total { get; set; }

        /// <summary>
        /// List of items returned by the search.
        /// </summary>
        [JsonProperty("items")]
        public List<Item> Items { get; set; } 
    }

    /// <summary>
    /// Root of an item object, this is used internally as Jagex's Shitty JSON API requires it.
    /// </summary>
    internal class ItemRoot
    {
        /// <summary>
        /// Internal item object contained within the root.
        /// </summary>
        [JsonProperty("item")]
        public Item Item { get; set; }
    }


}
