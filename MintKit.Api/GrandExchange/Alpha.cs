﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintKit.Api.GrandExchange
{
    /// <summary>
    /// Category Informatioon containing the number of items a category has given the starting letter.
    /// </summary>
    public class Alpha
    {
        public string letter { get; set; }
        public int items { get; set; }

    }

    /// <summary>
    /// Internal Alpha Root as Jagexs shitty API requires a root object, internally the MintKit API will bypass the root object and just return what is required.
    /// </summary>
    internal class AlphaRoot
    {
        public List<object> types { get; set; }
        public List<Alpha> alpha { get; set; }
    }
}
