﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MintKit.Common.Logging;

namespace MintKit.Api.Hiscores
{
    /// <summary>
    /// Hiscores API. Retrieves Information regarding a players Hiscores.
    /// </summary>
    public class HiscoresApi
    {

        /// <summary>
        /// Get a URL to a hiscores page given a player name.
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        internal static string HISCORES_URL(string player)
        {
            return string.Format("http://hiscore.runescape.com/index_lite.ws?player={0}", player);
        }

        /// <summary>
        /// The amount of skills.
        /// </summary>
        private static int SKILL_COUNT = 26;

        /// <summary>
        /// The amount of Activities (minigames).
        /// </summary>
        private static int ACTIVITIES_COUNT = 24;

        /// <summary>
        /// Get the name of a skill from its ID on the hiscores page.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static string SkillFromId(int id)
        {
            string name = "";
            switch (id)
            {
                case 0:name = "overall"; break;
                case 1:name = "attack"; break;
                case 2:name = "defence"; break;
                case 3:name = "strength"; break;
                case 4:name = "constitution"; break;
                case 5:name = "ranged"; break;
                case 6:name = "prayer"; break;
                case 7:name = "magic"; break;
                case 8:name = "cooking"; break;
                case 9:name = "woodcutting"; break;
                case 10:name = "fletching"; break;
                case 11:name = "fishing"; break;
                case 12:name = "firemaking"; break;
                case 13:name = "crafting"; break;
                case 14:name = "smithing"; break;
                case 15:name = "mining"; break;
                case 16:name = "herblore"; break;
                case 17:name = "agility"; break;
                case 18:name = "thieving"; break;
                case 19:name = "slayer"; break;
                case 20:name = "farming"; break;
                case 21:name = "runecrafting"; break;
                case 22:name = "hunter"; break;
                case 23:name = "construction"; break;
                case 24:name = "summoning"; break;
                case 25:name = "dungeoneering"; break;
                case 26:name = "divination"; break;
                default:name = "unknown"; break;
            }
            return name;
        }

        /// <summary>
        /// Get the name of a minigame given its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static string ActivityFromId(int id)
        {
            string name = "";
            switch (id){
            case 1:name = "bountyHunters"; break;
            case 2:name = "bountyHuntersRogues"; break;
            case 3:name = "dominionTower"; break;
            case 4:name = "theCrucible"; break;
            case 5:name = "castlewarsGames"; break;
            case 6:name = "baAttackers"; break;
            case 7:name = "baDefenders"; break;
            case 8:name = "baCollectors"; break;
            case 9:name = "baHealers"; break;
            case 10:name = "duelTournament"; break;
            case 11:name = "mobilisingArmies"; break;
            case 12:name = "conquest"; break;
            case 13:name = "fistOfGuthix"; break;
            case 14:name = "ggRessourceRace"; break;
            case 15:name = "ggAthletics"; break;
            case 16:name = "we2Alc"; break;
            case 17:name = "we2Blc"; break;
            case 18:name = "we2Apk"; break;
            case 19:name = "we2Bpk"; break;
            case 20:name = "hgl"; break;
            case 21:name = "hrl"; break;
            case 22:name = "cfb5ga"; break;
            case 23:name = "af15Ct"; break;
            case 24:name = "af15Rk"; break;
                default:name = "unknown"; break;
            }
            return name;
        }

        /// <summary>
        /// Retrieve a List of Skill information for a given player, this is not an Asynchronous task, and will stop the thread till its completed.
        /// Albeit with modern computers this shouldn't take more than a second.
        /// </summary>
        /// <param name="ply"></param>
        /// <returns></returns>
        /// TODO: Make an asynchrounous method
        public static List<Skill> GetHiscoresForPlayer(string ply)
        {
            List<Skill> hiscores = new List<Skill>();
            try
            {
                using (WebClient client = new WebClient())
                {
                    var data = client.OpenRead(HISCORES_URL(ply));
                    var reader = new StreamReader(data);

                    string linebuffer;
                    int line = 0;

                    while ((linebuffer = reader.ReadLine()) != null)
                    {

                        string[] splitBuffer = linebuffer.Split(',');
                        if (line < SKILL_COUNT + 1)
                        {
                            string name = SkillFromId(line);
                            int rank = Convert.ToInt32(splitBuffer[0]);
                            int level = Convert.ToInt32(splitBuffer[1]);
                            int exp = Convert.ToInt32(splitBuffer[2]);
                            hiscores.Add(new Skill() {Name = name, Experience = exp, Level = level, Rank = rank});
                            line++;
                        }
                        else
                        {
                            int activityId = line - SKILL_COUNT;
                            string name = ActivityFromId(activityId);
                            int rank = Convert.ToInt32(splitBuffer[0]);
                            int score = Convert.ToInt32(splitBuffer[1]);
                            hiscores.Add(new Skill() {Name = name, Experience = score, Rank = rank});
                            line++;
                        }
                    }
                    return hiscores;
                }
            }
            catch (Exception exception)
            {
                Log.GetLoggerFor(typeof (HiscoresApi).FullName).Warn("Unable to obtain hiscores: {0}", exception.Message);
                return null;
            }
        }
    }
}
