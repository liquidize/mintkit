﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MintKit.Api.Hiscores
{
    /// <summary>
    /// Skill Information.
    /// </summary>
    public class Skill
    {
        /// <summary>
        /// Players Rank in this skill.
        /// </summary>
        public int Rank { get; set; }
        /// <summary>
        /// Players Level (or total level if overall) in this skill.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Players Experience in this skill.
        /// </summary>
        public int Experience { get; set; }

        /// <summary>
        /// Name of the Skill.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// URI to the skill icon. This is internally set by the MintKit Client and is not accessible inside custom plugins.
        /// TODO: Make this accessible inside custom plugins
        /// </summary>
        public Uri SkillIcon { get; set; }
    }
}
