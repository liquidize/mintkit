﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MintKit.Api.Bestiary;

namespace BeastCreator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<BeastNameId> beastsList;
        private Beast currentBeast;

        public MainWindow()
        {
            InitializeComponent();

            statsBox.Items.Add("Defence");
            statsBox.Items.Add("Attack");
            statsBox.Items.Add("Magic");
            statsBox.Items.Add("Ranged");
            statsBox.Items.Add("Size");
            statsBox.Items.Add("Experience");
            statsBox.SelectedIndex = 0;

            statsBox.SelectionChanged += StatsBoxOnSelectionChanged;
        }

        private void StatsBoxOnSelectionChanged(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            if (currentBeast != null)
            {
                if (statsBox.SelectedIndex == 0)
                {
                    statsValBox.Text = currentBeast.Defence.ToString();
                }
                if (statsBox.SelectedIndex == 1)
                {
                    statsValBox.Text = currentBeast.Attack.ToString();
                }
                if (statsBox.SelectedIndex == 2)
                {
                    statsValBox.Text = currentBeast.Magic.ToString();
                }
                if (statsBox.SelectedIndex == 3)
                {
                    statsValBox.Text = currentBeast.Ranged.ToString();
                }
                if (statsBox.SelectedIndex == 4)
                {
                    statsValBox.Text = currentBeast.Size.ToString();
                }
                if (statsBox.SelectedIndex == 5)
                {
                    statsValBox.Text = currentBeast.Experience.ToString();
                }
            }
        }

        private async void searchBtn_click(object sender, RoutedEventArgs e)
        {
            resultsListBox.Items.Clear();
            beastsList = await BestiaryApi.GetBeastsByTermAsync(searchText.Text);
            if (beastsList != null)
            {
                for (int i = 0; i < beastsList.Count; i++)
                {
                    resultsListBox.Items.Add(beastsList[i].Name);
                }
            }

        }

        private async void resultsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (resultsListBox.SelectedIndex == -1) return;
            if (beastsList[resultsListBox.SelectedIndex] != null)
            {
                currentBeast = await BestiaryApi.GetBeastById(beastsList[resultsListBox.SelectedIndex].Id);
                if (currentBeast != null)
                {
                    nameBox.Text = currentBeast.Name;
                    examineBox.Text = currentBeast.Description;
                    levelBox.Text = currentBeast.Level.ToString();
                    aggressiveBox.IsChecked = currentBeast.Aggressive;
                    poisonousBox.IsChecked = currentBeast.Poisonous;
                    attackableBox.IsChecked = currentBeast.Attackable;
                    membersBox.IsChecked = currentBeast.Members;
                    statsBox.SelectedIndex = 0;
                    statsValBox.Text = currentBeast.Defence.ToString();
                }
            }
        }

        private void itemSearchBtn_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
