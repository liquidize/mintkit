﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MintKit.Common.Logging;

namespace BeastCreator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // Initialize Logging Engine
            Log.InitializeWith<Log4NetLog>();

            
            base.OnStartup(e);
        }
    }
}
