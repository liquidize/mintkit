﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Utilities;

namespace MintKit.DBMaker
{
    public class CropGrowthInfo
    {
        public string CropName { get; set; }
        public int CycleCount { get; set; }
        public int CycleTime { get; set; }
    }


    class Program
    {
        static List<CropGrowthInfo> CropInfos { get; set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to MintKit.DBMaker, where we make MintKit Data files!");

            Start:
            Console.WriteLine("Please enter the type of data file you want to make.");
            Console.WriteLine("1.Farming Data (Enter 1)");
            Console.WriteLine("Type serialize to serialize the data.");

            string dtype = Console.ReadLine();

            if (dtype != "1")
            {
                Console.WriteLine("Invalid Data Type!");
                goto Start;
            }
            else
            {
                Console.WriteLine("We are now creating Crop Data!");

                if (File.Exists("CropData.mkd"))
                {
                    string bson = File.ReadAllText("CropData.mkd");
                    byte[] bdata = Convert.FromBase64String(bson);
                    MemoryStream memStream = new MemoryStream(bdata);
                    BsonReader reader = new BsonReader(memStream);
                    reader.ReadRootValueAsArray = true;
                    JsonSerializer deserializer = new JsonSerializer();
                    CropInfos = deserializer.Deserialize<List<CropGrowthInfo>>(reader);
                    Console.WriteLine("Deserialized CropInfos with {0} crops!",CropInfos.Count);
                }
                else
                {
                    CropInfos = new List<CropGrowthInfo>();
                }
            }


            do
            {
                while (!Console.KeyAvailable)
                {

                    if (dtype == "1")
                    {
                        Console.WriteLine("Please insert data in the format CropName;CycleCount;TimePerCycle");
                        Console.WriteLine("Example: Potato;4;10");

                        string data = Console.ReadLine();

                        if (data == "serialize")
                        {
                            MemoryStream stream = new MemoryStream();
                            BsonWriter writer = new BsonWriter(stream);
                            JsonSerializer serializer = new JsonSerializer();
                            serializer.Serialize(writer, CropInfos);
                            File.WriteAllText("CropData.mkd", Convert.ToBase64String(stream.ToArray()));
                        }
                        else
                        {


                            string[] dSplit = data.Split(';');

                            if (dSplit.Length != 3)
                            {
                                Console.WriteLine("Invalid Data Format.");
                            }
                            else
                            {
                                CropInfos.Add(new CropGrowthInfo()
                                {
                                    CropName = dSplit[0],
                                    CycleCount = Convert.ToInt32(dSplit[1]),
                                    CycleTime = Convert.ToInt32(dSplit[2])
                                });
                            }
                        }
                    }

                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);



        }
    }
}
