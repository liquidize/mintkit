﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace MintKit.DBValidator
{
    public enum CropType
    {
        Allotment,
        Flowers,
        Hops,
        Herbs,
        Bushes,
        WoodTrees,
        FruitTrees,
        Cacti,
        Special
    }

    public class CropGrowthInfo
    {
        public string CropName { get; set; }
        public CropType CropType { get; set; }
        public int CycleCount { get; set; }
        public int CycleTime { get; set; }
    }

    class Program
    {
        static List<CropGrowthInfo> ReferenceList = new List<CropGrowthInfo>(); 

        static void Main(string[] args)
        {
            Console.WriteLine("Validating Crop Data File.");
            Console.WriteLine("Reading Data String.");
            string data = File.ReadAllText("CropData.mkd");
            byte[] bytes = Convert.FromBase64String(data);
            if (bytes.Length > 0)
            {
                Console.WriteLine("Valid MKD Data Format.");
            }
            else
            {
                Console.WriteLine("Invalid MKD Data Format.");
                Console.Read();
            }

            MemoryStream stream = new MemoryStream(bytes);
            BsonReader reader = new BsonReader(stream);
            reader.ReadRootValueAsArray = true;
            JsonSerializer deserializer = new JsonSerializer();
            List<CropGrowthInfo> dataList = null;
            try
            {
                dataList = deserializer.Deserialize<List<CropGrowthInfo>>(reader);
                if (dataList.Count > 0)
                {
                    Console.WriteLine("Valid MKD Data Object.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("InValid MKD Data Object.");
                Console.Read();
            }
            Console.WriteLine("Populating Reference Copy.");
            for (int i = 0; i < dataList.Count; i++)
            {
                ReferenceList.Add(dataList[i]);
            }

            Console.WriteLine("Checking Reference Copy For Duplicates and Errors.");

            int duplicates = 0;
            for (int i = 0; i < ReferenceList.Count; i++)
            {
                if (ReferenceList.Where(a => a.CropName == ReferenceList[i].CropName).ToList().Count > 1)
                {
                    duplicates += 1;
                }
            }
            Console.WriteLine("Number of Corrupted: 0");
            Console.WriteLine("Number of Duplicates: {0}",duplicates);
            if (duplicates == 0)
            {
                Console.WriteLine("Sec did good job, no corruption or duplicates found.");
            }

            for (int i = 0; i < 10; i++)
            {
                ReferenceList[i].CropType = CropType.Allotment;
            }
            for (int i = 10; i < 17; i++)
            {
                ReferenceList[i].CropType = CropType.Flowers;
            }
            for (int i = 17; i < 26; i++)
            {
                ReferenceList[i].CropType = CropType.Hops;
            }
            for (int i = 26; i < 44; i++)
            {
                ReferenceList[i].CropType = CropType.Herbs;
            }
            for (int i = 44; i < 52; i++)
            {
                ReferenceList[i].CropType = CropType.Bushes;
            }
            for (int i = 52; i < 57; i++)
            {
                ReferenceList[i].CropType = CropType.WoodTrees;
            }
            for (int i = 57; i < 64; i++)
            {
                ReferenceList[i].CropType = CropType.FruitTrees;
            }
            for (int i = 64; i < 67; i++)
            {
                ReferenceList[i].CropType = CropType.Cacti;
            }
            for (int i = 67; i < 74; i++)
            {
                ReferenceList[i].CropType = CropType.Special;
            }
            File.WriteAllText("CropData.json",JsonConvert.SerializeObject(ReferenceList,Formatting.Indented));

            Console.Read();

        }
    }
}
