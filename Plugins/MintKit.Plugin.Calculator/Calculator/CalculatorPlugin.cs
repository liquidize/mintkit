﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MintKit.Api;
using MintKit.Api.Plugin;
using MintKit.Plugin.Calculator.Properties;

namespace MintKit.Client.Core.Plugins
{
   public class CalculatorPlugin : IMintKitPluginBase
    {
       public MintKitPluginType PluginType
       {
           get { return MintKitPluginType.SecondaryTab; }
       }

       public string PluginName
       {
           get { return "Calculator"; }
       }

       public string PluginIcon
       {
           get { return "Resources\\images\\calculator.png"; }
       }

       public string PluginCategory
       {
           get { return "Tools"; }
       }

       public string PluginAuthor
       {
           get { return "MintKit"; }
       }

       public string Version
       {
           get { return "1.0.0"; }
       }

       public string Description
       {
           get { return "Calculates things."; }
       }

       public UserControl Control
       {
           get { return new CalculatorPluginControl(); }
       }
        

       public void Init()
       {
           MintKitCoreApi.IncludeResource(new ResourceDictionary() {Source = new Uri("pack://application:,,,/MintKit.Plugin.Calculator;component/Calculator/Control/Themes/Generic.xaml", UriKind.Absolute) });
       }

       public void DeInit()
       {
       }

       public void InstanceClosed(UserControl instance)
       {
           
       }
    }
}
