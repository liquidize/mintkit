﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MintKit.Api.GrandExchange;

namespace MintKit.Plugin.PriceChecker.PriceChecker
{
    /// <summary>
    /// Interaction logic for MoreInfoWindow.xaml
    /// </summary>
    public partial class MoreInfoWindow : Window
    {
        public MoreInfoWindow()
        {
            InitializeComponent();
        }

        public MoreInfoWindow(Item item)
        {
            InitializeComponent();

            nameLbl.Content = item.Name;
            typeLbl.Content = item.Type;
            descLbl.Text = item.Description;

            currentTrendLbl.Content = item.Current.Trend;
            currentPriceLbl.Content = item.Current.Price;

            todayTrendLbl.Content = item.Today.Trend;
            todayPriceLbl.Content = item.Today.Price;

            day30TrendLbl.Content = item.Day30.Trend;
            day30PriceLbl.Content = item.Day30.Change;

            day90TrendLbl.Content = item.Day90.Trend;
            day90PriceLbl.Content = item.Day90.Change;
        }
    }
}
