﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MintKit.Api;
using MintKit.Api.GrandExchange;
using MintKit.Common.Logging;
using MintKit.Plugin.PriceChecker.PriceChecker;

namespace MintKit.Client.Core.Plugins.PriceChecker
{
    /// <summary>
    /// Interaction logic for PriceCheckerControl.xaml
    /// </summary>
    public partial class PriceCheckerControl : UserControl
    {
        private DateTime timeLastChecked = DateTime.Now;
        private Item currentItem = null;

        public PriceCheckerControl()
        {
            InitializeComponent();
        }

        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            itemListBox.Items.Clear();

            string[] items = GrandExchangeApi.GetItemNamesFromString(itemNameBox.Text);

            for (int i = 0; i < items.Length; i++)
            {
                itemListBox.Items.Add(items[i]);
            }
            
        }

        private async void itemListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (itemListBox.SelectedItem != null)
            {
                if (timeLastChecked.AddSeconds(5) > DateTime.Now) return;
                int id = 0;
                if (GrandExchangeApi.ItemInformation.ContainsKey(itemListBox.SelectedItem.ToString()))
                {
                    id = GrandExchangeApi.ItemInformation[itemListBox.SelectedItem.ToString()];
                    Task<Item> item = GrandExchangeApi.GetItemDetailsAsync(id);

                    await item;
                    timeLastChecked = DateTime.Now;
                    if (item.IsFaulted || item.IsCanceled || item.Exception != null)
                    {
                        this.Log().Warn("Failed to obtain Item data for: {0}", itemListBox.SelectedItems.ToString());
                        if (MintKitCoreApi.GetDebugMode())
                        {
                            if (item.Exception != null) this.Log().Warn(item.Exception.Message);
                        }
                    }
                    else
                    {
                        if (item.Result != null)
                        {
                            currentItem = item.Result;
                            currentLbl.Content = item.Result.Current.Price;
                            todayLbl.Content = item.Result.Today.Price;
                            currentTrendLbl.Content = item.Result.Current.Trend;
                            todayTrendLbl.Content = item.Result.Today.Trend;

                            if (item.Result.Today.Trend.Contains("positive"))
                            {
                                todayTrendLbl.Foreground = new SolidColorBrush(Color.FromRgb(25,200,20));
                            }
                            else if (item.Result.Today.Trend.Contains("negative"))
                            {
                                todayTrendLbl.Foreground = new SolidColorBrush(Color.FromRgb(200, 25, 25));
                            }
                            else
                            {
                                todayTrendLbl.Foreground = new SolidColorBrush(Color.FromRgb(250,250,0));
                            }


                            if (item.Result.Current.Trend.Contains("positive"))
                            {
                                currentTrendLbl.Foreground = new SolidColorBrush(Color.FromRgb(25, 200, 20));
                            }
                            else if (item.Result.Current.Trend.Contains("negative"))
                            {
                                currentTrendLbl.Foreground = new SolidColorBrush(Color.FromRgb(200, 25, 25));
                            }
                            else
                            {
                                currentTrendLbl.Foreground = new SolidColorBrush(Color.FromRgb(250, 250, 0));
                            }




                            if (File.Exists(string.Format("Cache\\grandexchange\\icons\\{0}.gif", item.Result.Id)))
                            {
                                BitmapImage image = new BitmapImage();
                                image.BeginInit();
                                image.CacheOption = BitmapCacheOption.OnLoad;
                                image.StreamSource = new FileStream(string.Format("Cache\\grandexchange\\icons\\{0}.gif", item.Result.Id), FileMode.Open);
                                image.EndInit();
                                iconImage.Source = image;
                                iconImage.Stretch = Stretch.UniformToFill;

                            }
                            else
                            {
                                if (item.Result.IconLarge != "")
                                {
                                    BitmapImage img = new BitmapImage(new Uri(item.Result.IconLarge, UriKind.Absolute));
                                    byte[] data;
                                    using (WebClient client = new WebClient())
                                    {
                                        data = client.DownloadData(new Uri(item.Result.IconLarge, UriKind.Absolute));
                                    }

                                    iconImage.Source = img;
                                    iconImage.Stretch = Stretch.UniformToFill;

                                    if (MintKitCoreApi.GetCacheMode())
                                    {
                                        if (Directory.Exists("Cache\\grandexchange\\icons") != true)
                                        {
                                            Directory.CreateDirectory("Cache\\grandexchange\\icons");
                                        }


                                        File.WriteAllBytes(string.Format("Cache\\grandexchange\\icons\\{0}.gif", item.Result.Id), data);
                                    }
                                }
                            }


                        }
                    }
                }
            }
        }

        private void moreInfoBtn_Click(object sender, RoutedEventArgs e)
        {
            if (currentItem == null) return;
            MoreInfoWindow win = new MoreInfoWindow(currentItem);
            win.Show();
        }
    }
}
