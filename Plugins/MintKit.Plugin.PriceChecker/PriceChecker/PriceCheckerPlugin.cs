﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using MintKit.Api.Plugin;

namespace MintKit.Client.Core.Plugins.PriceChecker
{
    public class PriceCheckerPlugin : IMintKitPluginBase
    {
        public MintKitPluginType PluginType
        {
            get
            {
                return MintKitPluginType.TeritaryTab;

            }
        }

        public string PluginName
        {
            get { return "Price Checker"; }
        }

        public string PluginIcon
        {
            get { return "Resources\\images\\pricecheckericon.png"; }
        }

        public string PluginCategory
        {
            get { return "Economy"; }
        }

        public string PluginAuthor
        {
            get { return "MintKit"; }
        }

        public string Version
        {
            get { return "1.0.0"; }
        }

        public string Description
        {
            get
            {
                return
                    "Check the price of items on the grand exchange. This has a 5 second 'block' to prevent you from flooding Jagex servers. ";
            }
        }

        public UserControl Control
        {
            get { return new PriceCheckerControl(); }
        }
        

        public void Init()
        {
        }

        public void DeInit()
        {
        }

        public void InstanceClosed(UserControl instance)
        {
            
        }
    }
}
