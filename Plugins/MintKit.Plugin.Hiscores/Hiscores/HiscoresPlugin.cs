﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using MintKit.Api.Plugin;

namespace MintKit.Client.Core.Plugins.Hiscores
{
    public class HiscoresPlugin : IMintKitPluginBase
    {
        public MintKitPluginType PluginType
        {
            get { return MintKitPluginType.SecondaryTab; }
        }

        public string PluginName
        {
            get { return "Hiscores"; }
        }

        public string PluginIcon
        {
            get { return "Resources\\images\\hiscoresicon.png"; }
        }

        public string PluginCategory
        {
            get { return "Game"; }
        }

        public string PluginAuthor
        {
            get { return "MintKit"; }
        }

        public string Version
        {
            get { return "1.0.0"; }
        }

        public string Description
        {
            get { return "Get hiscores for people"; }
        }

        public UserControl Control
        {
            get { return new HiscoresPluginControl();}
        }

        public void Init()
        {
        }

        public void DeInit()
        {
        }

        public void InstanceClosed(UserControl instance)
        {
        }
    }
}
