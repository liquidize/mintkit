﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MintKit.Api.Hiscores;

namespace MintKit.Client.Core.Plugins.Hiscores
{
    /// <summary>
    /// Interaction logic for HiscoresPluginControl.xaml
    /// </summary>
    public partial class HiscoresPluginControl : UserControl
    {
      

        public List<Skill> RetrievedSkills { get; set; }
    

        public HiscoresPluginControl()
        {
            InitializeComponent();
            RetrievedSkills = new List<Skill>();
        }

      

        private void getHiscoresBtn_Click(object sender, RoutedEventArgs e)
        {
            RetrievedSkills.Clear();
            var hiscores = HiscoresApi.GetHiscoresForPlayer(nameBox.Text);
            if (hiscores != null)
            {
                for (int i = 0; i < hiscores.Count; i++)
                {
                    if (File.Exists(string.Format(@"../../../Resources/images/skills/{0}.png", hiscores[i].Name)))
                    {
                        hiscores[i].SkillIcon =
                            new Uri(string.Format(@"../../../Resources/images/skills/{0}.png", hiscores[i].Name),
                                UriKind.RelativeOrAbsolute);
                    }
                    else
                    {
                        hiscores[i].SkillIcon = new Uri(@"../../../Resources/images/skills/overall.png", UriKind.Relative);
                    };
                    RetrievedSkills.Add(hiscores[i]);
                }
                skillsView.ItemsSource = RetrievedSkills;
            }
        }
    }
}
