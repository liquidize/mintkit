﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace MintKit.Plugin.FarmTimers
{

    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class FarmTimerControl : UserControl
    {
        private TimerControl activeControl;

        private List<string> _woodTreeLocations = new List<string>() { "Lumbridge", "Varrock", "Falador", "Taverly", "Gnome Stronghold", "Prifddinas" };
        private List<string> _fruitTreeLocations = new List<string>() { "Tree Gnome Village", "Catherby", "Brimhaven", "Lletya", "Gnome Stronghold", "Herblore Habitat", "Prifddinas" };
        private List<string> _allotmentLocations = new List<string>() { "South Falador (1)", "South Falador (2)", "Port Phasmatys (1)", "Port Phasmatys (2)", "Catherby (1)", "Catherby (2)", "Ardougne (1)", "Ardougne (2)", "Harmony Island", "Taverly (Potato Only)" };
        private List<string> _flowerLocations = new List<string>()
        {
            "South Falador",
            "Port Phasmatys",
            "Catherby",
            "Ardougne",
            "Wilderness"
        };
        private List<string> _herbLocations = new List<string>()
        {
            "South Falador",
            "Port Phasmatys",
            "Catherby",
            "Ardougne",
            "Troll Stronghold",
            "Prifddinas"
        };

        private List<string> _hopsLocations = new List<string>()
        {
            "Lumbridge",
            "McGrubor's Wood",
            "Yanille",
            "Entrana",
        };

        private List<string> _bushLocations = new List<string>()
        {
            "Champions Guild",
            "Rimmington",
            "Ardougne",
            "Etceteria",
            "Prifddinas"
        };

        private List<string> _specialLocations = new List<string>()
        {
            "Etceteria (Spirit Tree)",
            "Port Sarim (Spirit Tree)",
            "Brimhaven (Spirit Tree)",
            "Canifis (Mushroom)",
            "Tirannwn (Mushroom)",
            "Draynor Manor (Evil Turnip)",
            "Al Kharid (Cactus)",
            "Draynor Manor (Belladonna)",
            "Tai Bwo Wannai (Calquat)",
            "Prifddinas (Elder Tree)",
            "Ardougne (Jade Vine)"
        };

        private Dictionary<string, List<string>> locations = new Dictionary<string, List<string>>();

        public FarmTimerControl()
        {
            InitializeComponent();

            // Setup locations dictionary, to make this sphagetti code, less sphagetti like down below...
            locations.Add("Wood", _woodTreeLocations);
            locations.Add("Fruit", _fruitTreeLocations);
            locations.Add("Herbs", _herbLocations);
            locations.Add("Bushes", _bushLocations);
            locations.Add("Flowers", _flowerLocations);
            locations.Add("Hops", _hopsLocations);
            locations.Add("Special", _specialLocations);
            locations.Add("Allotments", _allotmentLocations);

            // Setup locations in tabcontrols.
            SetupLocations();

            // Loaded saved instances, if they exists.
            InitializeSavedInstances();
        }


        private void InitializeSavedInstances()
        {
            if (FarmTimerPlugin.TimerInstances != null && FarmTimerPlugin.TimerInstances.Count > 0)
            {
                for (int i = 0; i < FarmTimerPlugin.TimerInstances.Count; i++)
                {

                    StackPanel stack = GetStackByType(FarmTimerPlugin.TimerInstances[i].Info.CropType);
                    TimerControl control =
                        stack.FindChild<TimerControl>(FarmTimerPlugin.TimerInstances[i].Location.Replace(" ", "").Replace("'", "").Replace("(", "").Replace(")", "") +
                                       FarmTimerPlugin.TimerInstances[i].Info.CropType);
                    if (control != null)
                    {
                        control.Instance = FarmTimerPlugin.TimerInstances[i];
                        control.InitializeTimer(control.Instance.Info);
                       
                    }
                }
            }
        }


        public void SetupLocations()
        {

            foreach (KeyValuePair<string, List<string>> kvp in locations)
            {
                StackPanel stack = GetStackByCropTypeString(kvp.Key);

                for (int i = 0; i < kvp.Value.Count; i++)
                {
                    FarmTimerInstance instance = new FarmTimerInstance();
                    instance.Info = null;
                    //instance.StartTime = DateTime.Now;
                    instance.Location = kvp.Value[i];

                    TimerControl timerControl = new TimerControl(instance);
                    timerControl.ContextMenu = new ContextMenu();
                    timerControl.Name = instance.Location.Replace(" ", "").Replace("'","").Replace("(","").Replace(")","") + GetCropTypeByString(kvp.Key);

                    // Check if CropInfos is null, if it is, break.
                    if (FarmTimerPlugin.CropInfos != null && FarmTimerPlugin.CropInfos.Count > 0)
                    {
                        List<CropGrowthInfo> crops =
                            FarmTimerPlugin.CropInfos.Where(c => c.CropType == GetCropTypeByString(kvp.Key)).ToList();

                        if (crops.Count > 0)
                        {
                            for (int t = 0; t < crops.Count; t++)
                            {
                                MenuItem contxtMenuItem = new MenuItem();
                                contxtMenuItem.Header = crops[t].CropName;
                                contxtMenuItem.Click += TimerContextMenuItemOnClick;
                                timerControl.ContextMenu.Items.Add(contxtMenuItem);
                            }
                        }

                    }

                    timerControl.MouseRightButtonDown += TimerOnMouseRightButtonDown;
                    stack.Children.Add(timerControl);
                }
            }
        }

        private StackPanel GetStackByCropTypeString(string cropType)
        {
            switch (cropType)
            {
                case "Bushes":
                    return bushesStack;
                    break;
                case "Wood":
                    return woodStack;
                    break;
                case "Fruit":
                    return fruitStack;
                    break;
                case "Special":
                    return specialStack;
                    break;
                case "Hops":
                    return hopsStack;
                    break;
                case "Allotments":
                    return allotmentStack;
                    break;
                case "Herbs":
                    return herbStack;
                    break;
                case "Flowers":
                    return flowerStack;
                    break;
                        default:
                    return null;

            }

        }

        private StackPanel GetStackByType(CropType cropType)
        {
            switch (cropType)
            {
                case CropType.Bushes:
                    return bushesStack;
                    break;
                case CropType.WoodTrees:
                    return woodStack;
                    break;
                case CropType.FruitTrees:
                    return fruitStack;
                    break;
                case CropType.Special:
                    return specialStack;
                    break;
                case CropType.Hops:
                    return hopsStack;
                    break;
                case CropType.Allotment:
                    return allotmentStack;
                    break;
                case CropType.Herbs:
                    return herbStack;
                    break;
                case CropType.Flowers:
                    return flowerStack;
                    break;
                default:
                    return null;

            }

        }

        private CropType GetCropTypeByString(string cropType)
        {
            switch (cropType)
            {
                case "Bushes":
                    return CropType.Bushes;
                    break;
                case "Wood":
                    return CropType.WoodTrees;
                    break;
                case "Fruit":
                    return CropType.FruitTrees;
                    break;
                case "Special":
                    return CropType.Special;
                    break;
                case "Hops":
                    return CropType.Hops;
                    break;
                case "Allotments":
                    return CropType.Allotment;
                    break;
                case "Herbs":
                    return CropType.Herbs;
                    break;
                case "Flowers":
                    return CropType.Flowers;
                    break;
                default:
                    return CropType.WoodTrees;

            }
        }

        private void TimerContextMenuItemOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (activeControl != null)
            {
                MenuItem item = sender as MenuItem;
                CropGrowthInfo info = FarmTimerPlugin.CropInfos.FirstOrDefault(a => a.CropName == (string)item.Header);
                if (info != null)
                {
                    activeControl.Instance.StartTime = DateTime.Now;
                    activeControl.InitializeTimer(info);
                    FarmTimerPlugin.TimerInstances.Add(activeControl.Instance);
                }
            }
        }

        private void TimerOnMouseRightButtonDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {

            TimerControl timercontrol = sender as TimerControl;
            if (timercontrol == null || timercontrol.GetType() != typeof(TimerControl)) return;

            activeControl = timercontrol;
            activeControl.ContextMenu.IsOpen = true;
        }
    }
}
