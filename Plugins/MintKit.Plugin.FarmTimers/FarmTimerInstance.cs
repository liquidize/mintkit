﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintKit.Plugin.FarmTimers
{
    public class FarmTimerInstance
    {
        public CropGrowthInfo Info { get; set; }
        public string Location { get; set; }
        public DateTime StartTime { get; set; }
    }
}
