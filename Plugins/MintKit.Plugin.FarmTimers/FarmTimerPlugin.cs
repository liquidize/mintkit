﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MintKit.Api;
using MintKit.Api.Plugin;
using MintKit.Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace MintKit.Plugin.FarmTimers
{
    public class FarmTimerPlugin : IMintKitPluginBase
    {
        public static List<FarmTimerInstance> TimerInstances { get; set; }
        public static List<CropGrowthInfo> CropInfos = new List<CropGrowthInfo>();

        public UserControl Control
        {
            get { return new FarmTimerControl(); }
        }

        public string Description
        {
            get { return "Keep track of when you plant stuff."; }
        }

        public string PluginAuthor
        {
            get { return "MintKit"; }
        }

        public string PluginCategory
        {
            get { return "Game"; }
        }

        public string PluginIcon
        {
            get { return "Resources\\images\\farmtimersicon.png"; }
        }

        public string PluginName
        {
            get { return "Farm Timers"; }
        }

        public MintKitPluginType PluginType
        {
            get
            {
                return MintKitPluginType.TeritaryTab;

            }
        }

        public string Version
        {
            get { return "1.0.0"; }
        }

        public void DeInit()
        {
            if (TimerInstances.Count > 0)
            {
                try
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        BsonWriter writer = new BsonWriter(stream);
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(writer,TimerInstances);


                        string bsonstring = Convert.ToBase64String(stream.ToArray());

                        // write directory if not exists
                        if (Directory.Exists(@"Resources\data\farmtimers\") != true)
                            Directory.CreateDirectory(@"Resources\data\farmtimers");

                        File.WriteAllText(@"Resources\data\farmtimers\farmtimers.mkd",bsonstring);
                        this.Log().Info("Wrote Farm Timers Data.");
                    }
                }
                catch (Exception ex)
                {
                    if (MintKitCoreApi.GetDebugMode())
                    {
                        this.Log().Warn(ex.ToString);
                    }
                }
            }
        }

        public async void Init()
        {
            if (File.Exists(@"Resources\data\farmtimers\farmtimers.mkd"))
            {
                try
                {
                    string data = File.ReadAllText(@"Resources\data\farmtimers\farmtimers.mkd");
                    byte[] bytes = Convert.FromBase64String(data);

                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        BsonReader bson = new BsonReader(ms);
                        JsonSerializer serializer = new JsonSerializer();
                        bson.ReadRootValueAsArray = true;
                        TimerInstances = serializer.Deserialize<List<FarmTimerInstance>>(bson);
                        this.Log().Info("Loaded Farm Timers.");
                    }
                }
                catch (Exception ex)
                {
                    this.Log().Warn(ex.Message);
                }
            }
            else
            {
                TimerInstances = new List<FarmTimerInstance>();
            }

            if (File.Exists(@"Resources\data\farmtimers\cropgrowthinfo.mkd"))
            {
                string cropdata = File.ReadAllText(@"Resources\data\farmtimers\cropgrowthinfo.mkd");
                try
                {
                    CropInfos = JsonConvert.DeserializeObject<List<CropGrowthInfo>>(cropdata);
                }
                catch (JsonException jsonex)
                {
                    CropInfos = new List<CropGrowthInfo>();
                    if (MintKitCoreApi.GetDebugMode())
                    {
                        this.Log().Warn("Could not deserialize CropGrowthInfo.");
                        this.Log().Error(jsonex.ToString());
                    }

                }
            }
            else
            {
                Assembly assem = this.GetType().Assembly;

                // write the embeded cropdata to disk.
                using (Stream stream = assem.GetManifestResourceStream("MintKit.Plugin.FarmTimers.CropData.mkd"))
                {
                    byte[] buffer = new byte[stream.Length];
                    int t = await stream.ReadAsync(buffer, 0, (int)stream.Length);

                    if (Directory.Exists(@"Resources\data\farmtimers\") != true)
                        Directory.CreateDirectory(@"Resources\data\farmtimers");

                    File.WriteAllBytes(@"Resources\data\farmtimers\cropgrowthinfo.mkd", buffer);
                }
                // Now continue and reattempt.
                string cropdata = File.ReadAllText(@"Resources\data\farmtimers\cropgrowthinfo.mkd");
                try
                {
                    CropInfos = JsonConvert.DeserializeObject<List<CropGrowthInfo>>(cropdata);
                }
                catch (JsonException jsonex)
                {
                    CropInfos = new List<CropGrowthInfo>();
                    if (MintKitCoreApi.GetDebugMode())
                    {
                        this.Log().Warn("Could not deserialize CropGrowthInfo.");
                        this.Log().Error(jsonex.ToString());
                    }

                }

            }

        }

        public void InstanceClosed(UserControl instance)
        {

        }
    }
}
