﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MintKit.Plugin.FarmTimers
{
    /// <summary>
    /// Interaction logic for TimerControl.xaml
    /// </summary>
    public partial class TimerControl : UserControl
    {
        private FarmTimerInstance _instance;
        private DispatcherTimer _timer;
        private DateTime EndTime;
        private int _minutesSinceStage = 0;
        private int secondssincelastmin = 0;
        private int stage = 0;

        public FarmTimerInstance Instance { get { return _instance; } set { _instance = value; } }

        public TimerControl()
        {
            InitializeComponent();
        }

        public TimerControl(FarmTimerInstance instance)
        {
            InitializeComponent();

            _instance = instance;
            locationLbl.Content = _instance.Location;
            
        }

        public void InitializeTimer(CropGrowthInfo info)
        {
            _instance.Info = info;

            cropLbl.Content = _instance.Info.CropName;

            stageLbl.Content = "0/" + _instance.Info.CycleCount;

            timeleftLbl.Content =
                (_instance.StartTime - TimeSpan.FromMinutes(_instance.Info.CycleTime * _instance.Info.CycleCount))
                    .ToString("HH:mm:ss");

            EndTime = _instance.StartTime + TimeSpan.FromMinutes(_instance.Info.CycleTime * _instance.Info.CycleCount);

            _timer = new DispatcherTimer(TimeSpan.FromSeconds(1), DispatcherPriority.Background, TimerCallback,
                this.Dispatcher);
            _timer.Start();
        }

        private void AdjustTimerControl()
        {
            // Is completed?
            if (DateTime.Now >= EndTime)
            {
                stageLbl.Content = _instance.Info.CycleCount + "/" + _instance.Info.CycleCount;
                timeleftLbl.Content = (DateTime.Now - EndTime).ToString(@"hh\:mm\:ss");
                _timer.Stop();
                return;
            }

            TimeSpan timeSinceStart = (DateTime.Now - _instance.StartTime);
            stage = timeSinceStart.Minutes/_instance.Info.CycleTime;

            if (stage > _instance.Info.CycleCount)
                stage = _instance.Info.CycleCount;


            stageLbl.Content = stage + "/" + _instance.Info.CycleCount;
            timeleftLbl.Content = (EndTime - DateTime.Now).ToString(@"hh\:mm\:ss");

            if ((EndTime - DateTime.Now).Minutes == 0 && (EndTime - DateTime.Now).Seconds <= 5)
            {
                SystemSounds.Beep.Play();
            }

        }


        private void TimerCallback(object sender, EventArgs eventArgs)
        {
            AdjustTimerControl();
        }
    }
}
