﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintKit.Plugin.FarmTimers
{
    public enum CropType
    {
        Allotment,
        Flowers,
        Hops,
        Herbs,
        Bushes,
        WoodTrees,
        FruitTrees,
        Cacti,
        Special
    }

    public class CropGrowthInfo
    {
        public string CropName { get; set; }
        public CropType CropType { get; set; }
        public int CycleCount { get; set; }
        public int CycleTime { get; set; }
    }
}
